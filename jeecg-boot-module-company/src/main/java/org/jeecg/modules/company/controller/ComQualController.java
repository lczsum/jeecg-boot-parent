package org.jeecg.modules.company.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.company.entity.ComQual;
import org.jeecg.modules.company.service.IComInfoService;
import org.jeecg.modules.company.service.IComQualService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 企业资质
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Api(tags="企业资质")
@RestController
@RequestMapping("/company/comQual")
@Slf4j
public class ComQualController extends JeecgController<ComQual, IComQualService> {
	@Autowired
	private IComQualService comQualService;
	 @Autowired
	 private IComInfoService comInfoService;

	/**
	 * 分页列表查询
	 *
	 * @param comQual
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "企业资质-分页列表查询")
	@ApiOperation(value="企业资质-分页列表查询", notes="企业资质-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ComQual comQual,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		String token = req.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("查看失败，用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		comQual.setCompanyId(id);
		QueryWrapper<ComQual> queryWrapper = QueryGenerator.initQueryWrapper(comQual, req.getParameterMap());
		Page<ComQual> page = new Page<ComQual>(pageNo, pageSize);
		IPage<ComQual> pageList = comQualService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param comQual
	 * @return
	 */
	@AutoLog(value = "企业资质-添加")
	@ApiOperation(value="企业资质-添加", notes="企业资质-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ComQual comQual,HttpServletRequest req) {
		String token = req.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("查看失败，用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		comQual.setCompanyId(id);
		comQualService.save(comQual);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param comQual
	 * @return
	 */
	@AutoLog(value = "企业资质-编辑")
	@ApiOperation(value="企业资质-编辑", notes="企业资质-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ComQual comQual) {
		comQualService.updateById(comQual);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "企业资质-通过id删除")
	@ApiOperation(value="企业资质-通过id删除", notes="企业资质-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		comQualService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "企业资质-批量删除")
	@ApiOperation(value="企业资质-批量删除", notes="企业资质-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.comQualService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "企业资质-通过id查询")
	@ApiOperation(value="企业资质-通过id查询", notes="企业资质-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ComQual comQual = comQualService.getById(id);
		if(comQual==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(comQual);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param comQual
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ComQual comQual) {
        return super.exportXls(request, comQual, ComQual.class, "企业资质");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ComQual.class);
    }

}
