package org.jeecg.modules.company.service.impl;

import org.jeecg.modules.company.entity.ComInnovate;
import org.jeecg.modules.company.mapper.ComInnovateMapper;
import org.jeecg.modules.company.service.IComInnovateService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科技创新
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
@Service
public class ComInnovateServiceImpl extends ServiceImpl<ComInnovateMapper, ComInnovate> implements IComInnovateService {

}
