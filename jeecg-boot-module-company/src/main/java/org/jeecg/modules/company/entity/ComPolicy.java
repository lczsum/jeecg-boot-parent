package org.jeecg.modules.company.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 政策扶持
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Data
@TableName("com_policy")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="com_policy对象", description="政策扶持")
public class ComPolicy implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@Excel(name = "创建日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
	@Excel(name = "更新人", width = 15)
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@Excel(name = "更新日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**政策名*/
	@Excel(name = "政策名", width = 15)
    @ApiModelProperty(value = "政策名")
    private String policyname;
	/**级别*/
	@Excel(name = "级别", width = 15,dicCode = "policylevel")
    @ApiModelProperty(value = "级别")
    @Dict(dicCode = "policylevel")
    private Integer level;
	/**部门*/
	@Excel(name = "部门", width = 15,dicCode = "policydept")
    @ApiModelProperty(value = "部门")
    @Dict(dicCode = "policydept")
    private String deptname;
	/**类别*/
	@Excel(name = "类别", width = 15,dicCode = "policycatg")
    @ApiModelProperty(value = "类别")
    @Dict(dicCode = "policycatg")
    private Integer ptype;
	/**时效性*/
	@Excel(name = "时效性", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "时效性")
    private Date timeliness;
	/**文件*/
	@Excel(name = "文件", width = 15)
    @ApiModelProperty(value = "文件")
    private String files;
	/**政策详情*/
	@Excel(name = "政策详情", width = 15)
    @ApiModelProperty(value = "政策详情")
    private String details;
	/**发布时间*/
	@Excel(name = "发布时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "发布时间")
    private Date createtime;
    /**申报时间*/
    @Excel(name = "申报时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "申报时间")
    private Date reporttime;
	/**是否截止*/
	@Excel(name = "是否截止", width = 15,dicCode = "iscut")
    @ApiModelProperty(value = "是否截止")
    @Dict(dicCode = "iscut")
    private Integer iscut;
    /**对应企业*/
    @Excel(name = "企业id", width = 15)
    @ApiModelProperty(value = "企业id")
    private String companyId;
    /**开始享受时间*/
    @Excel(name = "开始享受时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "开始享受时间")
    private Date starttime;
    /**结束享受时间*/
    @Excel(name = "结束享受时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "结束享受时间")
    private Date stoptime;
    /**享受金额*/
    @Excel(name = "享受金额", width = 15)
    @ApiModelProperty(value = "享受金额")
    private String amount;
    /**文件名*/
    @Excel(name = "文件名", width = 15)
    @ApiModelProperty(value = "文件名")
    private String filename;
    /**备注*/
    @Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String remark;
    /**项目名称*/
    @Excel(name = "项目名称", width = 15)
    @ApiModelProperty(value = "项目名称")
    private String pname;

}
