package org.jeecg.modules.company.service;

import org.jeecg.modules.company.entity.ComHonor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 品牌荣誉
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface IComHonorService extends IService<ComHonor> {

}
