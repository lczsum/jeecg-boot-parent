package org.jeecg.modules.company.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 科技创新
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
@Data
@TableName("com_innovate")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="com_innovate对象", description="科技创新")
public class ComInnovate implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
//	@Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
//	@Excel(name = "创建日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
//	@Excel(name = "更新人", width = 15)
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
//	@Excel(name = "更新日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**企业id*/
	@Excel(name = "企业id", width = 15)
    @ApiModelProperty(value = "企业id")
    private String companyId;
	/**所属部门*/
	@Excel(name = "所属部门", width = 15)
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**创新类别*/
	@Excel(name = "创新类别", width = 15,dicCode = "innovationcatg")
    @ApiModelProperty(value = "创新类别")
    @Dict(dicCode = "innovationcatg")
    private String innovatetype;
	/**创新内容*/
	@Excel(name = "创新内容", width = 15)
    @ApiModelProperty(value = "创新内容")
    private String innovatecontent;
	/**合作开始时间*/
	@Excel(name = "合作开始时间", width = 15)
    @ApiModelProperty(value = "合作开始时间")
    private String coostarttime;
	/**合作结束时间*/
	@Excel(name = "合作结束时间", width = 15)
    @ApiModelProperty(value = "合作结束时间")
    private String coostoptime;
	/**合作院校*/
	@Excel(name = "合作院校", width = 15)
    @ApiModelProperty(value = "合作院校")
    private String coocolleges;
	/**合作专家*/
	@Excel(name = "合作专家", width = 15)
    @ApiModelProperty(value = "合作专家")
    private String cooexpert;
	/**项目名称*/
	@Excel(name = "项目名称", width = 15)
    @ApiModelProperty(value = "项目名称")
    private String cooproject;
}
