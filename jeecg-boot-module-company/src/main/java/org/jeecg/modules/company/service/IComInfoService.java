package org.jeecg.modules.company.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.company.entity.ComInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * @Description: 企业基本信息表
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface IComInfoService extends IService<ComInfo> {
    /**
     * 根据用户查询用户id
     * @param username
     * @return
     */
    public String getByUserName(String username);

    /**
     *
     * @param page
     * @param paramMap
     * @return
     */
    public IPage<ComInfo> listByKey(Page<ComInfo> page,Map paramMap);

    /**
     * 查询企业基本信息
     * @param id
     * @return
     */
    public JSONObject getComInfoById(String id);

    /**
     * 查询企业用户
     * @param paramMap
     * @return
     */
    public IPage<Map> getComUser(Map paramMap);

    /**
     * 启用
     * @param paramMap
     * @return
     */
    public JSONObject startCom(Map paramMap);
    /**
     * 停用
     * @param paramMap
     * @return
     */
    public JSONObject stopCom(Map paramMap);

    /**
     * 查询企业列表
     * @return
     */
    public JSONObject getComName();

    /**
     * 删除企业
     * @return
     */
    public JSONObject delByIds(Collection<? extends Serializable> idList);

}
