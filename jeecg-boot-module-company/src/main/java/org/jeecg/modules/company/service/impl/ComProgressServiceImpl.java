package org.jeecg.modules.company.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.company.entity.ComProgress;
import org.jeecg.modules.company.mapper.ComProgressMapper;
import org.jeecg.modules.company.service.DictCoverService;
import org.jeecg.modules.company.service.IComProgressService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 办理进度
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Service
public class ComProgressServiceImpl extends ServiceImpl<ComProgressMapper, ComProgress> implements IComProgressService {

    @Resource
    private ComProgressMapper comProgressMapper;//企业进度
    @Resource
    private DictCoverService dictCoverService;//字典转换
    /**
     * 检索
     * @param paramMap
     * @return
     */
    @Override
    public IPage<Map> listByKey(Map paramMap) {
        JSONObject resultObject=new JSONObject();
        IPage<Map> page = new Page<>(Integer.valueOf(paramMap.get("pageNo").toString()), Integer.valueOf(paramMap.get("pageSize").toString()));
        List resultList=comProgressMapper.listByKey(page,paramMap);
        List resultList2=new ArrayList();
        //字典转换
        List<String> paramList=new ArrayList();
        paramList.add("processprogress");
        paramList.add("processresult");
        paramList.add("applylicense");
        List<Map> dictList=dictCoverService.getDictCode(paramList);
        for(int i=0;i<resultList.size();i++){
            Map tempMap=(Map)resultList.get(i);
            for(int j=0;j<dictList.size();j++){
                Map dictMap=dictList.get(j);
                String dict_code=dictMap.get("dict_code").toString();
                String item_text=dictMap.get("item_text").toString();
                String item_value=dictMap.get("item_value").toString();
                //办理进度
                if("processprogress".equals(dict_code)&&item_value.equals(tempMap.get("progress")==null?null:tempMap.get("progress").toString())){
                    tempMap.put("progress_dictText",item_text);
                    continue;
                }
                //办理结果
                if("processresult".equals(dict_code)&&item_value.equals(tempMap.get("idpass")==null?null:tempMap.get("idpass").toString())){
                    tempMap.put("idpass_dictText",item_text);
                }
                //办理证照
                if("applylicense".equals(dict_code)&&item_value.equals(tempMap.get("applylicense")==null?null:tempMap.get("applylicense").toString())){
                    tempMap.put("applylicense_dictText",item_text);
                }

            }
            tempMap.put("companyId",tempMap.get("company_id"));
            resultList2.add(tempMap);
        }
        page.setRecords(resultList2);
        return page;
    }

    /**
     * 管理员查看进度
     * @param paramMap
     * @return
     */
    @Override
    public IPage<Map> adminListByKey(Map paramMap) {
        JSONObject resultObject=new JSONObject();
        IPage<Map> page = new Page<>(Integer.valueOf(paramMap.get("pageNo").toString()), Integer.valueOf(paramMap.get("pageSize").toString()));
        List resultList=comProgressMapper.adminListByKey(page,paramMap);
        List resultList2=new ArrayList();
        //字典转换
        List<String> paramList=new ArrayList();
        paramList.add("processprogress");
        paramList.add("processresult");
        paramList.add("applylicense");
        List<Map> dictList=dictCoverService.getDictCode(paramList);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for(int i=0;i<resultList.size();i++){
            Map tempMap=(Map)resultList.get(i);
            for(int j=0;j<dictList.size();j++){
                Map dictMap=dictList.get(j);
                String dict_code=dictMap.get("dict_code").toString();
                String item_text=dictMap.get("item_text").toString();
                String item_value=dictMap.get("item_value").toString();
                //办理进度
                if("processprogress".equals(dict_code)&&item_value.equals(tempMap.get("progress")==null?null:tempMap.get("progress").toString())){
                    tempMap.put("progress_dictText",item_text);
                    continue;
                }
                //办理结果
                if("processresult".equals(dict_code)&&item_value.equals(tempMap.get("idpass")==null?null:tempMap.get("idpass").toString())){
                    tempMap.put("idpass_dictText",item_text);
                }
                //办理证照
                if("applylicense".equals(dict_code)&&item_value.equals(tempMap.get("applylicense")==null?null:tempMap.get("applylicense").toString())){
                    tempMap.put("applylicense_dictText",item_text);
                }
            }
            tempMap.put("companyId",tempMap.get("company_id"));
            resultList2.add(tempMap);
        }
        page.setRecords(resultList2);
        return page;
    }
}
