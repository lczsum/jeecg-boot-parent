package org.jeecg.modules.company.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.entity.ComProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 上下游项目
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface ComProjectMapper extends BaseMapper<ComProject> {
    /**
     * 根据用户username查询上下游项目
     * @param parmaMap
     * @return
     */
    public List selectByCreateUser(Map parmaMap);
}
