package org.jeecg.modules.company.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.company.entity.ComPolicy;
import org.jeecg.modules.company.service.IComInfoService;
import org.jeecg.modules.company.service.IComPolicyService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 政策扶持
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Api(tags="政策扶持")
@RestController
@RequestMapping("/company/comPolicy")
@Slf4j
public class ComPolicyController extends JeecgController<ComPolicy, IComPolicyService> {
	@Autowired
	private IComPolicyService comPolicyService;
	 @Autowired
	 private IComInfoService comInfoService;
	/**
	 * 分页列表查询
	 *
	 * @param comPolicy
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "政策扶持-分页列表查询")
	@ApiOperation(value="政策扶持-分页列表查询", notes="政策扶持-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ComPolicy comPolicy,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ComPolicy> queryWrapper = QueryGenerator.initQueryWrapper(comPolicy, req.getParameterMap());
		Page<Map> page = new Page<Map>(pageNo, pageSize);
		IPage<Map> pageList = comPolicyService.listComPolicy(page, comPolicy);
		return Result.ok(pageList);
	}
	 /**
	  * 查询自己企业政策扶持信息
	  * @param pageNo
	  * @param pageSize
	  * @param req
	  * @return
	  */
	 @AutoLog(value = "政策扶持-查询自己企业政策扶持信息")
	 @ApiOperation(value="政策扶持-查询自己企业政策扶持信息", notes="政策扶持-查询自己企业政策扶持信息")
	 @GetMapping(value = "/getComOwnList")
	 public Result<?> getComOwnList(ComPolicy comPolicy,
									@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									HttpServletRequest req) {
	 	//获取用户信息
	 	 String token = req.getHeader("X-Access-Token");
		 if(oConvertUtils.isEmpty(token)) {
			 return Result.error("查看失败，用户不存在！");
		 }
		 String username = JwtUtil.getUsername(token);
		 Map paramMap =new HashMap();

		 paramMap.put("pageNo",pageNo);
		 paramMap.put("pageSize",pageSize);
		 paramMap.put("policyname",comPolicy.getPolicyname());
		 String id=comInfoService.getByUserName(username);
		 paramMap.put("id",id);
		 IPage<Map> result=comPolicyService.getComOwnList(paramMap);
		 return Result.ok(result);
	 }
	/**
	 * 添加
	 * @param comPolicy
	 * @return
	 */
	@AutoLog(value = "政策扶持-添加")
	@ApiOperation(value="政策扶持-添加", notes="政策扶持-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ComPolicy comPolicy) {
		comPolicyService.save(comPolicy);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param comPolicy
	 * @return
	 */
	@AutoLog(value = "政策扶持-编辑")
	@ApiOperation(value="政策扶持-编辑", notes="政策扶持-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ComPolicy comPolicy) {
		comPolicyService.updateById(comPolicy);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "政策扶持-通过id删除")
	@ApiOperation(value="政策扶持-通过id删除", notes="政策扶持-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		comPolicyService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "政策扶持-批量删除")
	@ApiOperation(value="政策扶持-批量删除", notes="政策扶持-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.comPolicyService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "政策扶持-通过id查询")
	@ApiOperation(value="政策扶持-通过id查询", notes="政策扶持-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ComPolicy comPolicy = comPolicyService.getById(id);
		if(comPolicy==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(comPolicy);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param comPolicy
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ComPolicy comPolicy) {
        return super.exportXls(request, comPolicy, ComPolicy.class, "政策扶持");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ComPolicy.class);
    }

}
