package org.jeecg.modules.company.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.jeecg.modules.company.entity.ComTrz;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Description: 投融资需求
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface IComTrzService extends IService<ComTrz> {
    /**
     * 投融资检索信息
     * @param paramMap
     * @return
     */
    IPage<Map> getTrzByKey(Map paramMap);
}
