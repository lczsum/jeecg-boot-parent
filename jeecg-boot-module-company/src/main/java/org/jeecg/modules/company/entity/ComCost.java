package org.jeecg.modules.company.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 企业费用
 * @Author: jeecg-boot
 * @Date:   2020-04-25
 * @Version: V1.0
 */
@Data
@TableName("com_cost")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="com_cost对象", description="企业费用")
public class ComCost implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@Excel(name = "创建日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
	@Excel(name = "更新人", width = 15)
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@Excel(name = "更新日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
	@Excel(name = "所属部门", width = 15)
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**年份*/
	@Excel(name = "年份", width = 15, format = "yyyy")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy")
    @DateTimeFormat(pattern="yyyy")
    @ApiModelProperty(value = "年份")
    private Date years;
	/**水用量*/
	@Excel(name = "水用量", width = 15)
    @ApiModelProperty(value = "水用量")
    private String wateramount;
	/**水费用*/
	@Excel(name = "水费用", width = 15)
    @ApiModelProperty(value = "水费用")
    private String watercharge;
	/**电用量*/
	@Excel(name = "电用量", width = 15)
    @ApiModelProperty(value = "电用量")
    private String eleamount;
	/**电费用*/
	@Excel(name = "电费用", width = 15)
    @ApiModelProperty(value = "电费用")
    private String elecharge;
	/**气用量*/
	@Excel(name = "气用量", width = 15)
    @ApiModelProperty(value = "气用量")
    private String gasamount;
	/**气费用*/
	@Excel(name = "气费用", width = 15)
    @ApiModelProperty(value = "气费用")
    private String gascharge;
    /**对应企业*/
    @Excel(name = "企业id", width = 15)
    @ApiModelProperty(value = "企业id")
    private String companyId;
}
