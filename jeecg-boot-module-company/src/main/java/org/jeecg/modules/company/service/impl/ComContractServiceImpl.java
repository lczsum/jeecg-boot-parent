package org.jeecg.modules.company.service.impl;

import org.jeecg.modules.company.entity.ComContract;
import org.jeecg.modules.company.mapper.ComContractMapper;
import org.jeecg.modules.company.service.IComContractService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 投资合同
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Service
public class ComContractServiceImpl extends ServiceImpl<ComContractMapper, ComContract> implements IComContractService {

}
