package org.jeecg.modules.company.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.entity.ComQual;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 企业资质
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface ComQualMapper extends BaseMapper<ComQual> {
    /**
     * 根据用户username查询企业资质信息
     * @param parmaMap
     * @return
     */
    public List selectByCreateUser(Map parmaMap);

}
