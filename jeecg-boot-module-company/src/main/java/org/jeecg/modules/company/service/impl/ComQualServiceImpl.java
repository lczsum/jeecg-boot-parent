package org.jeecg.modules.company.service.impl;

import org.jeecg.modules.company.entity.ComQual;
import org.jeecg.modules.company.mapper.ComQualMapper;
import org.jeecg.modules.company.service.IComQualService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 企业资质
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Service
public class ComQualServiceImpl extends ServiceImpl<ComQualMapper, ComQual> implements IComQualService {

}
