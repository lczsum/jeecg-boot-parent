package org.jeecg.modules.company.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.entity.ComPolicy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 政策扶持
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface ComPolicyMapper extends BaseMapper<ComPolicy> {
    /**
     * 查询企业自己的政策扶持
     * @param paramMap
     * @return
     */
    public List getComOwnList(IPage<Map> page,@Param("map") Map paramMap);

    /**
     * 政策详情
     * @param paramMap
     * @return
     */
    public List selectByCreateUser(Map paramMap);

    /**
     *查询查询企业政策扶持
     * @param page
     * @param comPolicy
     * @return
     */
    public  List<Map> listComPolicy(Page<Map> page, @Param("entity") ComPolicy comPolicy);

}
