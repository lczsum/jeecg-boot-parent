package org.jeecg.modules.company.mapper;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.entity.ComInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 企业基本信息表
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface ComInfoMapper extends BaseMapper<ComInfo> {
    /**
     * 根据创建者查询企业id
     * @param username
     * @return
     */
    public String getByUserName(String username);

    /**
     * 查询企业
     * @param page
     * @param paramMap
     * @return
     */
    public List<ComInfo> listByKey(IPage<ComInfo> page, @Param("entity")Map paramMap);

    /**
     * 查询企业基本信息
     * @param id
     * @return
     */
    public Map selectComInfoById(String id);

    /**
     * 查询企业用户
     * @param paramMap
     * @return
     */
    public List getComUser(IPage<Map> page,@Param("map") Map paramMap);

    /**
     * 启用企业
     * @param paramMap
     * @return
     */
    public Integer  startCom(Map paramMap);
    /**
     * 停用企业
     * @param paramMap
     * @return
     */
    public Integer  stopCom(Map paramMap);

    /**
     * 查询企业列表
     * @return
     */
    public List getComName();

    /**
     * 删除企业
     * @param idList
     * @return
     */
    public Integer delByIds(Collection<? extends Serializable> idList);

    /**
     * 禁用用户
     * @param idList
     * @return
     */
    public Integer setUserStatus(Collection<? extends Serializable> idList);
}
