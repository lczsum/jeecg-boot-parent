package org.jeecg.modules.company.service;

import org.jeecg.modules.company.entity.ComQual;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 企业资质
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface IComQualService extends IService<ComQual> {

}
