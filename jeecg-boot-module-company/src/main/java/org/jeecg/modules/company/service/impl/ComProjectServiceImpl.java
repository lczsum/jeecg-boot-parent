package org.jeecg.modules.company.service.impl;

import org.jeecg.modules.company.entity.ComProject;
import org.jeecg.modules.company.mapper.ComProjectMapper;
import org.jeecg.modules.company.service.IComProjectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 上下游项目
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Service
public class ComProjectServiceImpl extends ServiceImpl<ComProjectMapper, ComProject> implements IComProjectService {

}
