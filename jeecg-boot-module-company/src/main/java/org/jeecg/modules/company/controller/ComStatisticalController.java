package org.jeecg.modules.company.controller;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.modules.company.service.ComStatisticalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Description: 统计类
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Api(tags="统计数据")
@RestController
@RequestMapping("/company/statistical")
@Slf4j
public class ComStatisticalController {

    @Autowired
    private ComStatisticalService comStatisticalService;
    /**
     * 首页统计
     *
     * @return
     */
    @AutoLog(value = "企业-首页统计")
    @ApiOperation(value="企业-首页统计", notes="企业-首页统计")
    @GetMapping(value = "/getIndexStatustical")
    public Result<?> getIndexStatustical(){
        JSONObject resultObject=new JSONObject();
        resultObject=comStatisticalService.getIndexStatustical();
        return Result.ok(resultObject);
    }

    /**
     * 企业数据分析
     * @return
     */
    @AutoLog(value = "企业-数据统计")
    @ApiOperation(value="企业-数据统计", notes="企业-数据统计")
    @GetMapping(value = "/getDataStatustical")
    public Result<?> getDataStatustical(@RequestParam Map paramMap){
        JSONObject resultObject=new JSONObject();
        resultObject=comStatisticalService.getDataStatustical(paramMap);
        return Result.ok(resultObject);
    }
}
