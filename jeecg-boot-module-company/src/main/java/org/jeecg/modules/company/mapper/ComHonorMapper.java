package org.jeecg.modules.company.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.entity.ComHonor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 品牌荣誉
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Mapper
public interface ComHonorMapper extends BaseMapper<ComHonor> {
    /**
     * 根据用户username查询荣誉信息
     * @param parmaMap
     * @return
     */
    public List selectByCreateUser(Map parmaMap);
}
