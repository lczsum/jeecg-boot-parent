package org.jeecg.modules.company.service.impl;

import org.jeecg.modules.company.entity.ComColdchain;
import org.jeecg.modules.company.mapper.ComColdchainMapper;
import org.jeecg.modules.company.service.IComColdchainService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 冷链物流
 * @Author: jeecg-boot
 * @Date:   2020-04-25
 * @Version: V1.0
 */
@Service
public class ComColdchainServiceImpl extends ServiceImpl<ComColdchainMapper, ComColdchain> implements IComColdchainService {

}
