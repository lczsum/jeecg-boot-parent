package org.jeecg.modules.company.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 企业人员
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Data
@TableName("com_user")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="com_user对象", description="企业人员")
public class ComUser implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**企业id*/
    @ApiModelProperty(value = "企业id")
    private String companyId;
	/**名称*/
	@Excel(name = "员工姓名", width = 15)
    @ApiModelProperty(value = "员工姓名")
    private String name;
	/**电话*/
	@Excel(name = "电话", width = 15)
    @ApiModelProperty(value = "电话")
    private String phonenum;
	/**人员类别*/
	@Excel(name = "员工类别", width = 15,dicCode = "personnelcatg")
    @ApiModelProperty(value = "员工类别")
    @Dict(dicCode = "personnelcatg")
    private Integer utype;
	/**政治面貌*/
	@Excel(name = "政治面貌", width = 15,dicCode = "politiccountenance")
    @ApiModelProperty(value = "政治面貌")
    @Dict(dicCode = "politiccountenance")
    private Integer politicsface;
	/**所属部门*/
	@Excel(name = "所属部门", width = 15)
    @ApiModelProperty(value = "所属部门")
    private String dept;
	/**工号*/
	@Excel(name = "工号", width = 15)
    @ApiModelProperty(value = "工号")
    private String jobnum;
	/**学历*/
	@Excel(name = "学历", width = 15,dicCode = "education")
    @ApiModelProperty(value = "学历")
    @Dict(dicCode = "education")
    private Integer education;
	/**生日*/
	@Excel(name = "生日", width = 20, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "生日")
    private Date birthday;
	/**年龄*/
	@Excel(name = "年龄", width = 15)
    @ApiModelProperty(value = "年龄")
    private String age;
	/**户籍*/
	@Excel(name = "户籍", width = 15)
    @ApiModelProperty(value = "户籍")
    private String domicile;
	/**现居地*/
    @Excel(name = "现居地", width = 15)
    @ApiModelProperty(value = "现居地")
    private String address;
    /**省*/
    @Excel(name = "省", width = 15)
    @ApiModelProperty(value = "省")
    private String province;
    /**城市*/
    @Excel(name = "城市", width = 15)
    @ApiModelProperty(value = "城市")
    private String city;
}
