package org.jeecg.modules.company.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.entity.ComContract;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 投资合同
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface ComContractMapper extends BaseMapper<ComContract> {

}
