package org.jeecg.modules.company.service.impl;

import org.jeecg.modules.company.entity.ComEconomic;
import org.jeecg.modules.company.mapper.ComEconomicMapper;
import org.jeecg.modules.company.service.IComEconomicService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description: 经济数据
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
@Service
public class ComEconomicServiceImpl extends ServiceImpl<ComEconomicMapper, ComEconomic> implements IComEconomicService {
   @Resource
   private ComEconomicMapper comEconomicMapper;
    /**
     * 判断该年是否有经济数据
     * @param comEconomic
     * @return
     */
    @Override
    public Boolean isYearEconomicExist(ComEconomic comEconomic) {
        List resultList=comEconomicMapper.isYearEconomicExist(comEconomic);
        if(resultList!=null && !resultList.isEmpty()){
            return true;
        }else{
            return false;
        }

    }
}
