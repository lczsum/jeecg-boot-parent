package org.jeecg.modules.company.service.impl;

import org.jeecg.modules.company.mapper.DictCoverMapper;
import org.jeecg.modules.company.service.DictCoverService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据字典处理
 */
@Service
public class DictCoverServiceImpl implements DictCoverService {
    @Resource
    private DictCoverMapper dictCoverMapper;
    /**
     * 根据数据字典类型和值查询对应的名称
     * @param codes
     * @return
     */
    @Override
    public List<Map> getDictCode(List codes) {
        List<Map> resultList=dictCoverMapper.getDictCode(codes);
        return resultList;
    }
}
