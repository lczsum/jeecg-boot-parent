package org.jeecg.modules.company.service;

import org.jeecg.modules.company.entity.ComColdchain;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 冷链物流
 * @Author: jeecg-boot
 * @Date:   2020-04-25
 * @Version: V1.0
 */
public interface IComColdchainService extends IService<ComColdchain> {

}
