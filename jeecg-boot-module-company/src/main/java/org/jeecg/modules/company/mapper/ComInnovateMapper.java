package org.jeecg.modules.company.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.entity.ComInnovate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科技创新
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */

public interface ComInnovateMapper extends BaseMapper<ComInnovate> {

    public List selectByCreateUser(Map paramMap);

}
