package org.jeecg.modules.company.service;

import org.jeecg.modules.company.entity.ComCost;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 企业费用
 * @Author: jeecg-boot
 * @Date:   2020-04-25
 * @Version: V1.0
 */
public interface IComCostService extends IService<ComCost> {

}
