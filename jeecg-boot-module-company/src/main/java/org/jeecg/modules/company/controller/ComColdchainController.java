package org.jeecg.modules.company.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.company.entity.ComColdchain;
import org.jeecg.modules.company.service.IComColdchainService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.company.service.IComInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 冷链物流
 * @Author: jeecg-boot
 * @Date:   2020-04-25
 * @Version: V1.0
 */
@Api(tags="冷链物流")
@RestController
@RequestMapping("/demo/comColdchain")
@Slf4j
public class ComColdchainController extends JeecgController<ComColdchain, IComColdchainService> {
	@Autowired
	private IComColdchainService comColdchainService;
	 @Autowired
	 private IComInfoService comInfoService;
	/**
	 * 分页列表查询
	 *
	 * @param comColdchain
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "冷链物流-分页列表查询")
	@ApiOperation(value="冷链物流-分页列表查询", notes="冷链物流-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ComColdchain comColdchain,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		String token = req.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("token校验未通过,用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		comColdchain.setCompanyId(id);

		QueryWrapper<ComColdchain> queryWrapper = QueryGenerator.initQueryWrapper(comColdchain, req.getParameterMap());
		Page<ComColdchain> page = new Page<ComColdchain>(pageNo, pageSize);
		IPage<ComColdchain> pageList = comColdchainService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param comColdchain
	 * @return
	 */
	@AutoLog(value = "冷链物流-添加")
	@ApiOperation(value="冷链物流-添加", notes="冷链物流-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ComColdchain comColdchain,HttpServletRequest req) {
		String token = req.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("token校验未通过,用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		comColdchain.setCompanyId(id);

		comColdchainService.save(comColdchain);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param comColdchain
	 * @return
	 */
	@AutoLog(value = "冷链物流-编辑")
	@ApiOperation(value="冷链物流-编辑", notes="冷链物流-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ComColdchain comColdchain) {
		comColdchainService.updateById(comColdchain);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "冷链物流-通过id删除")
	@ApiOperation(value="冷链物流-通过id删除", notes="冷链物流-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		comColdchainService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "冷链物流-批量删除")
	@ApiOperation(value="冷链物流-批量删除", notes="冷链物流-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.comColdchainService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "冷链物流-通过id查询")
	@ApiOperation(value="冷链物流-通过id查询", notes="冷链物流-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ComColdchain comColdchain = comColdchainService.getById(id);
		if(comColdchain==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(comColdchain);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param comColdchain
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ComColdchain comColdchain) {
        return super.exportXls(request, comColdchain, ComColdchain.class, "冷链物流");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ComColdchain.class);
    }

}
