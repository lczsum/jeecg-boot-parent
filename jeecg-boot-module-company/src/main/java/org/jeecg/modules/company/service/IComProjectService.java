package org.jeecg.modules.company.service;

import org.jeecg.modules.company.entity.ComProject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 上下游项目
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface IComProjectService extends IService<ComProject> {

}
