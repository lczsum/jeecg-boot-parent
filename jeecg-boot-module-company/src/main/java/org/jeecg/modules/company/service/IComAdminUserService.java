package org.jeecg.modules.company.service;

import java.util.List;

import org.jeecg.modules.company.dto.ComAdminUserDto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

public interface IComAdminUserService extends IService<ComAdminUserDto>{

	public  Page<ComAdminUserDto> page(ComAdminUserDto comAdminUserDto,Page<ComAdminUserDto> page) ;
	
	boolean save(ComAdminUserDto comAdminUserDto);

	public void deleteUser(String id);

	public void deleteBatchUsers(String ids);

	public void frozenBatch(List<String> list, String status);

	public List<ComAdminUserDto> findList(ComAdminUserDto dto);

	public void changePassword(ComAdminUserDto dto);

	public void edit(ComAdminUserDto dto);

	public void saveCompanyAdminUser(ComAdminUserDto dto);

	public ComAdminUserDto getUserCompanyByUserId();

	
}
