package org.jeecg.modules.company.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.company.entity.ComDamagingname;
import org.jeecg.modules.company.mapper.ComDamagingnameMapper;
import org.jeecg.modules.company.service.DictCoverService;
import org.jeecg.modules.company.service.IComDamagingnameService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description: 负面清单
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Service
public class ComDamagingnameServiceImpl extends ServiceImpl<ComDamagingnameMapper, ComDamagingname> implements IComDamagingnameService {

    @Resource
    private ComDamagingnameMapper comDamagingnameMapper;
    @Resource
    private DictCoverService dictCoverService;//字典转换
    /**
     * 检索负面清单
     * @param paramMap
     * @return
     */
    @Override
    public IPage<Map> listByKey(Map paramMap) {
        Page<Map> paramPage = new Page<>(Integer.valueOf(paramMap.get("pageNo").toString()), Integer.valueOf(paramMap.get("pageSize").toString()));
        List resultList=comDamagingnameMapper.listByKey(paramPage,paramMap);
        //paramPage.setRecords(resultList);
        List resultList2=new ArrayList();
        //字典转换
        List<String> paramList=new ArrayList();
        paramList.add("damagingtype");
        paramList.add("industry");
        List<Map> dictList=dictCoverService.getDictCode(paramList);
        for(int i=0;i<resultList.size();i++){
            Map tempMap=(Map)resultList.get(i);
            for(int j=0;j<dictList.size();j++){
                Map dictMap=dictList.get(j);
                String dict_code=dictMap.get("dict_code").toString();
                String item_text=dictMap.get("item_text").toString();
                String item_value=dictMap.get("item_value").toString();
                //行业
                if("industry".equals(dict_code)&&item_value.equals(tempMap.get("industry")==null?null:tempMap.get("industry").toString())){
                    tempMap.put("industry_dictText",item_text);
                    continue;
                }
                //负面类型
                if("damagingtype".equals(dict_code)&&item_value.equals(tempMap.get("damagingtype")==null?null:tempMap.get("damagingtype").toString())){
                    tempMap.put("damagingtype_dictText",item_text);
                }
            }
            tempMap.put("companyId",tempMap.get("company_id"));
            resultList2.add(tempMap);
        }
        paramPage.setRecords(resultList2);
        return paramPage;
    }

    /**
     * 关闭负面清单
     * @param paramMap
     * @return
     */
    @Override
    public JSONObject closeComDamagingname(Map paramMap) {
        JSONObject resultObject=new JSONObject();
        int result=comDamagingnameMapper.closeComDamagingname(paramMap);
        resultObject.put("result",true);
        resultObject.put("msg","关闭成功");
        return resultObject;
    }
}
