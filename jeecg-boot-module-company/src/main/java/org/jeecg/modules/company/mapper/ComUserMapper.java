package org.jeecg.modules.company.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.entity.ComUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 企业人员
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface ComUserMapper extends BaseMapper<ComUser> {
    /**
     * 检索企业人员
     * @param comUser
     * @return
     */
    public List<ComUser> listByKey(IPage<ComUser> page,@Param("entity") ComUser comUser);

    /**
     * 检索企业联系人
     * @param pramaMap
     * @return
     */
    public List<Map> selectByCreateUser(Map pramaMap);

    /**
     * 查询省
     * @return
     */
    public List getProvince();

    /**
     * 根据省id查询城市
     * @param paramMap
     * @return
     */
    public List getCityByPid(Map paramMap);

    /**
     * 法人校验
     * @param paramMap
     * @return
     */
    public List<ComUser> checkUnique(Map paramMap);

}
