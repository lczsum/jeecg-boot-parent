package org.jeecg.modules.company.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.dto.ComAdminUserDto;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

public interface ComAdminUserMapper extends BaseMapper<ComAdminUserDto>  {
	
	/**
	 *  分页查询
	 * @param page
	 * @param entity
	 * @return
	 */
	List<ComAdminUserDto> queryComAdminUserPage(Page<ComAdminUserDto> page, @Param("entity")ComAdminUserDto entity);

	/**
	 *  根据账号获取用户信息
	 * @param createBy
	 * @return
	 */
	//ComAdminUserDto getUserByUsername(@Param("createBy")String createBy);

	/**
	 *  根据用户编号获取对应企业信息
	 * @param id
	 * @return
	 */
	ComAdminUserDto getUserCompanyByUserId(@Param("id")String id);
	
	/**
	 *  保存用户和公司关系
	 * @param user
	 */
	void addUserWithCompany(@Param("dto")ComAdminUserDto dto);

	/**
	 *  获取用户角色信息
	 * @param id
	 * @return
	 */
	List<String> getUserRoleByUserId(@Param("id")String id);

	/**
	 *  保存用户角色关系
	 * @param user
	 */
	void addUserWithRole(@Param("user")ComAdminUserDto user);

	/**
	 * 保存用户
	 * @param user
	 */
	void insertSysUser(@Param("user")ComAdminUserDto user);

	/**
	 * 删除用户
	 * @param id
	 */
	void deleteUser(@Param("id")String id);
	
	/**
	 * 批量删除用户
	 * @param list
	 */
	void removeByIds(@Param("list")List<String> list);

	/**
	 * 冻结，解冻
	 * @param list
	 * @param status
	 */
	void frozenBatch(@Param("user")ComAdminUserDto user);

	/**
	 *  查询list
	 * @param dto
	 * @return
	 */
	List<ComAdminUserDto> findList(@Param("dto")ComAdminUserDto dto);

	/**
	 * 修改密码
	 * @param dto
	 */
	void changePassword(@Param("dto")ComAdminUserDto dto);

	/**
	 * 修改信息密码
	 * @param dto
	 */
	void edit(@Param("dto")ComAdminUserDto dto);

	

	
	
}
