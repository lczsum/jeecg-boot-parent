package org.jeecg.modules.company.service;

import org.jeecg.modules.company.entity.ComInnovate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科技创新
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
public interface IComInnovateService extends IService<ComInnovate> {

}
