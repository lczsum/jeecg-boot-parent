package org.jeecg.modules.company.service;

import org.jeecg.modules.company.entity.ComContract;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 投资合同
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface IComContractService extends IService<ComContract> {

}
