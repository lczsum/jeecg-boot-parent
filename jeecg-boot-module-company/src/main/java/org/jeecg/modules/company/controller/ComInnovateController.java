package org.jeecg.modules.company.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.company.dto.ComAdminUserDto;
import org.jeecg.modules.company.entity.ComInnovate;
import org.jeecg.modules.company.service.IComAdminUserService;
import org.jeecg.modules.company.service.IComInfoService;
import org.jeecg.modules.company.service.IComInnovateService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 科技创新
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
@Api(tags="科技创新")
@RestController
@RequestMapping("/company/comInnovate")
@Slf4j
public class ComInnovateController extends JeecgController<ComInnovate, IComInnovateService> {
	@Autowired
	private IComInnovateService comInnovateService;
	@Autowired
	private IComInfoService comInfoService;
	@Autowired
	private IComAdminUserService comAdminUserService;
	 
	/**
	 * 分页列表查询
	 *
	 * @param comInnovate
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "科技创新-分页列表查询")
	@ApiOperation(value="科技创新-分页列表查询", notes="科技创新-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ComInnovate comInnovate,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		String token = req.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("查看失败，用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		comInnovate.setCompanyId(id);
		QueryWrapper<ComInnovate> queryWrapper = QueryGenerator.initQueryWrapper(comInnovate, req.getParameterMap());
		Page<ComInnovate> page = new Page<ComInnovate>(pageNo, pageSize);
		IPage<ComInnovate> pageList = comInnovateService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param comInnovate
	 * @return
	 */
	@AutoLog(value = "科技创新-添加")
	@ApiOperation(value="科技创新-添加", notes="科技创新-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ComInnovate comInnovate,HttpServletRequest req) {
		String token = req.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("查看失败，用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		comInnovate.setCompanyId(id);
		comInnovateService.save(comInnovate);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param comInnovate
	 * @return
	 */
	@AutoLog(value = "科技创新-编辑")
	@ApiOperation(value="科技创新-编辑", notes="科技创新-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ComInnovate comInnovate) {
		comInnovateService.updateById(comInnovate);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "科技创新-通过id删除")
	@ApiOperation(value="科技创新-通过id删除", notes="科技创新-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		comInnovateService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "科技创新-批量删除")
	@ApiOperation(value="科技创新-批量删除", notes="科技创新-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.comInnovateService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "科技创新-通过id查询")
	@ApiOperation(value="科技创新-通过id查询", notes="科技创新-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ComInnovate comInnovate = comInnovateService.getById(id);
		if(comInnovate==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(comInnovate);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param comInnovate
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ComInnovate comInnovate) {
        return super.exportXls(request, comInnovate, ComInnovate.class, "科技创新");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
    	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            MultipartFile file = entity.getValue();// 获取上传文件对象
            ImportParams params = new ImportParams();
            //params.setTitleRows(0);
            params.setHeadRows(1);
            params.setNeedSave(true);
            try {
                List<ComInnovate> comInnovates = ExcelImportUtil.importExcel(file.getInputStream(), ComInnovate.class, params);
                //comInfoService.saveBatch(comInfos);
              //根据当前登录用户名获取所属企业。
                String companyId ="";
                ComAdminUserDto  dto = comAdminUserService.getUserCompanyByUserId();
                if(dto!=null) {
                	  companyId = dto.getCompanyId();
                }else {
                	return Result.error("您未归属于任何企业，导入失败.");
                }
                for (ComInnovate comInnovate : comInnovates) {
                	if(null != dto) {
                		comInnovate.setCompanyId(companyId);
                	}
                	comInnovateService.save(comInnovate);
                }
                return Result.ok("科技创新数据导入成功！数据行数：" + comInnovates.size());
            } catch (Exception e) {
                log.error(e.getMessage(),e);
                return Result.error("科技创新数据导入失败.");
            } finally {
                try {
                    file.getInputStream().close();
                } catch (IOException e) {
                	log.error(e.getMessage(), e);
                }
            }
        }
        return Result.error("科技创新数据导入失败！");
    }

}
