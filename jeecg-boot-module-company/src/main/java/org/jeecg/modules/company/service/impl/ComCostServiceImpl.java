package org.jeecg.modules.company.service.impl;

import org.jeecg.modules.company.entity.ComCost;
import org.jeecg.modules.company.mapper.ComCostMapper;
import org.jeecg.modules.company.service.IComCostService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 企业费用
 * @Author: jeecg-boot
 * @Date:   2020-04-25
 * @Version: V1.0
 */
@Service
public class ComCostServiceImpl extends ServiceImpl<ComCostMapper, ComCost> implements IComCostService {

}
