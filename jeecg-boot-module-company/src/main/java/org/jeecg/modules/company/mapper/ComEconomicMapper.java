package org.jeecg.modules.company.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.entity.ComEconomic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 经济数据
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
public interface ComEconomicMapper extends BaseMapper<ComEconomic> {
    /**
     * 查询企业经济数据
     * @param paramMap
     * @return
     */
    public List selectByCompanyId(Map paramMap);

    /**
     * 查询经济数据，判断是否录入该年的经济数据
     * @param comEconomic
     * @return
     */
    public List isYearEconomicExist(ComEconomic comEconomic);
}
