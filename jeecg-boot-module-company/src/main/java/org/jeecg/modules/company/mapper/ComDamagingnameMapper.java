package org.jeecg.modules.company.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.entity.ComDamagingname;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 负面清单
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface ComDamagingnameMapper extends BaseMapper<ComDamagingname> {
    /**
     * 检索负面清单
     * @param
     * @return
     */
    public List listByKey(IPage<Map> page,  @Param("map")Map paramMap);

    /**
     * 停用负面清单
     * @return
     */
    public Integer closeComDamagingname(Map paramMap);

    /**
     * 查询企业对应的负面清单
     * @param paramMap
     * @return
     */
    public List selectByCreateUser(Map paramMap);
}
