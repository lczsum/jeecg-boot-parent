package org.jeecg.modules.company.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.company.entity.ComUser;

import java.util.List;
import java.util.Map;

/**
 * 数据字典处理
 */
public interface DictCoverMapper extends BaseMapper<ComUser> {
    /**
     * 根据数据字典类型和值查询对应的名称
     * @param codes
     * @return
     */
    public List<Map> getDictCode(List<String> codes);
}
