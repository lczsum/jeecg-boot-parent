package org.jeecg.modules.company.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.company.entity.ComPolicy;
import org.jeecg.modules.company.mapper.ComPolicyMapper;
import org.jeecg.modules.company.service.DictCoverService;
import org.jeecg.modules.company.service.IComPolicyService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 政策扶持
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Service
public class ComPolicyServiceImpl extends ServiceImpl<ComPolicyMapper, ComPolicy> implements IComPolicyService {

    @Resource
    private ComPolicyMapper comPolicyMapper;
    @Resource
    private DictCoverService dictCoverService;//字典转换
    /**
     * 查询企业自己的政策扶持
     * @param paramMap
     * @return
     */
    @Override
    public IPage<Map> getComOwnList(Map paramMap) {
        JSONObject resultObject=new JSONObject();
        IPage<Map> page = new Page<>(Integer.valueOf(paramMap.get("pageNo").toString()), Integer.valueOf(paramMap.get("pageSize").toString()));
        List resultList=comPolicyMapper.getComOwnList(page,paramMap);
        List resultList2=new ArrayList();
        //字典转换
        List<String> paramList=new ArrayList();
        paramList.add("iscut");
        paramList.add("policycatg");
        paramList.add("policydept");
        paramList.add("policylevel");
        List<Map> dictList=dictCoverService.getDictCode(paramList);
        for(int i=0;i<resultList.size();i++){
            Map tempMap=(Map)resultList.get(i);
            for(int j=0;j<dictList.size();j++){
                Map dictMap=dictList.get(j);
                String dict_code=dictMap.get("dict_code").toString();
                String item_text=dictMap.get("item_text").toString();
                String item_value=dictMap.get("item_value").toString();
                //是否截止
                if("iscut".equals(dict_code)&&item_value.equals(tempMap.get("iscut")==null?null:tempMap.get("iscut").toString())){
                    tempMap.put("iscut_dictText",item_text);
                    continue;
                }
                //政策类别
                if("policycatg".equals(dict_code)&&item_value.equals(tempMap.get("ptype")==null?null:tempMap.get("ptype").toString())){
                    tempMap.put("ptype_dictText",item_text);
                }
                //政策级别
                if("policylevel".equals(dict_code)&&item_value.equals(tempMap.get("level")==null?null:tempMap.get("level").toString())){
                    tempMap.put("level_dictText",item_text);
                }
                //政策部门
                if("policydept".equals(dict_code)&&item_value.equals(tempMap.get("deptname")==null?null:tempMap.get("deptname").toString())){
                    tempMap.put("deptname_dictText",item_text);
                }

            }
            resultList2.add(tempMap);
        }
        page.setRecords(resultList);
        return page;
    }

    /**
     * 查询查询企业政策扶持
     * @param page
     * @param comPolicy
     * @return
     */
    @Override
    public IPage<Map> listComPolicy(Page<Map> page, ComPolicy comPolicy) {
       List<Map> resultList= comPolicyMapper.listComPolicy(page, comPolicy);
        page.setRecords(resultList);
        return page;
    }
}
