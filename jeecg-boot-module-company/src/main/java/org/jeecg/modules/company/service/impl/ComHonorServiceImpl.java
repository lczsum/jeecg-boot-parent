package org.jeecg.modules.company.service.impl;

import org.jeecg.modules.company.entity.ComHonor;
import org.jeecg.modules.company.mapper.ComHonorMapper;
import org.jeecg.modules.company.service.IComHonorService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 品牌荣誉
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Service
public class ComHonorServiceImpl extends ServiceImpl<ComHonorMapper, ComHonor> implements IComHonorService {

}
