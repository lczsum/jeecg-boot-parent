package org.jeecg.modules.company.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import freemarker.template.SimpleDate;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.company.entity.ComInfo;
import org.jeecg.modules.company.mapper.*;
import org.jeecg.modules.company.service.DictCoverService;
import org.jeecg.modules.company.service.IComInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Description: 企业基本信息表
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Service
public class ComInfoServiceImpl extends ServiceImpl<ComInfoMapper, ComInfo> implements IComInfoService {
    @Resource
    private ComInfoMapper comInfoMapper;//基本信息
    @Resource
    private ComHonorMapper comHonorMapper;//荣誉
    @Resource
    private ComInnovateMapper comInnovateMapper;//科技创新
    @Resource
    private ComQualMapper comQualMapper;//企业资质
    @Resource
    private ComProjectMapper comProjectMapper;//上下游项目
    @Resource
    private ComDamagingnameMapper comDamagingnameMapper;//负面清单
    @Resource
    private ComPolicyMapper comPolicyMapper;//政策详情
    @Resource
    private DictCoverService dictCoverService;//字典转换
    @Resource
    private ComUserMapper comUserMapper;//企业人员管理
    @Resource
    private ComEconomicMapper comEconomicMapper;//经济数据
    @Resource
    private ComTrzMapper comTrzMapper;//投融资信息
    /**
     * 根据用户查询企业信息id
     * @param username
     * @return
     */
    @Override
    public String getByUserName(String username) {
        String id=comInfoMapper.getByUserName(username);
        return id;
    }

    /**
     * 查询企业信息
     * @param page
     * @param paramMap
     * @return
     */
    @Override
    public IPage<ComInfo> listByKey(Page<ComInfo> page, Map paramMap) {
       List<ComInfo> resultList=comInfoMapper.listByKey(page,paramMap);
        page.setRecords(resultList);
        return page;
    }

    /**
     * 查询企业基本信息、营业执照、品牌荣誉、科技创新、企业资格认证、上下游项目、经济数据
     * @param id
     * @return
     */
    @Override
    public JSONObject getComInfoById(String id) {
        JSONObject resultObject=new JSONObject();
        Map paramMap=new HashMap();
        //获取用户所在公司
        paramMap.put("company_id",id);
        //基本信息,营业执照，产品大类，图片
        Map comInfoMap=comInfoMapper.selectComInfoById(id);
        if(comInfoMap.get("intime")!=null){
                comInfoMap.put("intime", DateUtils.dateformat(comInfoMap.get("intime").toString(),"yyyy-MM-dd"));
        }
        List<String> paramList=new ArrayList();
        paramList.add("industry");
        paramList.add("workshop");
        paramList.add("productcatg");
        paramList.add("honorcatg");
        paramList.add("innovationcatg");
        paramList.add("qualcatg");
        paramList.add("projectcatg");
        paramList.add("damagingtype");
        paramList.add("policylevel");
        paramList.add("iscut");
        paramList.add("policycatg");
        paramList.add("policydept");
        List<Map> dictList=dictCoverService.getDictCode(paramList);
        for(int i=0;i<dictList.size();i++){
            Map tempMap=dictList.get(i);
            String dict_code=tempMap.get("dict_code").toString();
            String item_text=tempMap.get("item_text").toString();
            String item_value=tempMap.get("item_value").toString();
            //设置行业
            if(dict_code.equals("industry")&&item_value.equals(comInfoMap.get("industry")==null?null:comInfoMap.get("industry").toString())){
                comInfoMap.put("industry_dictText",item_text);
                continue;
            }
            //设置厂房
            if(dict_code.equals("workshop")&&item_value.equals(comInfoMap.get("workshop")==null?null:comInfoMap.get("workshop").toString())){
                comInfoMap.put("workshop_dictText",item_text);
                continue;
            }
            //设置产品大类
            if(dict_code.equals("productcatg")&&item_value.equals(comInfoMap.get("productcategory")==null?null:comInfoMap.get("productcategory").toString())){
                comInfoMap.put("productcategory_dictText",item_text);
                continue;
            }

        }




        //品牌荣誉
        List honorList=comHonorMapper.selectByCreateUser(paramMap);
        List honorList2=new ArrayList();
        for(int i=0;i<honorList.size();i++){
            Map tempMap=(Map)honorList.get(i);
            for(int j=0;j<dictList.size();j++){
                Map codeMap=dictList.get(j);
                String dict_code=codeMap.get("dict_code").toString();
                String item_text=codeMap.get("item_text").toString();
                String item_value=codeMap.get("item_value").toString();
                if(dict_code.equals("honorcatg")&&item_value.equals(tempMap.get("honortype")==null?null:tempMap.get("honortype").toString())){
                    tempMap.put("honortype_dictText",item_text);
                    break;
                }
            }
            honorList2.add(tempMap);
        }
        //科技创新
        List innovateList= comInnovateMapper.selectByCreateUser(paramMap);
        List innovateList2=new ArrayList();
        for(int i=0;i<innovateList.size();i++){
            Map tempMap=(Map)innovateList.get(i);
            for(int j=0;j<dictList.size();j++){
                Map codeMap=dictList.get(j);
                String dict_code=codeMap.get("dict_code").toString();
                String item_text=codeMap.get("item_text").toString();
                String item_value=codeMap.get("item_value").toString();
                if(dict_code.equals("innovationcatg")&&item_value.equals(tempMap.get("innovatetype")==null?null:tempMap.get("innovatetype").toString())){
                    tempMap.put("innovatetype_dictText",item_text);
                    break;
                }
            }
            innovateList2.add(tempMap);
        }
        //企业资格认证
        List qualList= comQualMapper.selectByCreateUser(paramMap);
        List qualList2=new ArrayList();
        for(int i=0;i<qualList.size();i++){
            Map tempMap=(Map)qualList.get(i);
            for(int j=0;j<dictList.size();j++){
                Map codeMap=dictList.get(j);
                String dict_code=codeMap.get("dict_code").toString();
                String item_text=codeMap.get("item_text").toString();
                String item_value=codeMap.get("item_value").toString();
                if(dict_code.equals("qualcatg")&&item_value.equals(tempMap.get("qualtype")==null?null:tempMap.get("qualtype").toString())){
                    tempMap.put("qualtype_dictText",item_text);
                    break;
                }
            }
            qualList2.add(tempMap);
        }
        //上下游项目
        List projectList= comProjectMapper.selectByCreateUser(paramMap);
        List projectList2=new ArrayList();
        for(int i=0;i<projectList.size();i++){
            Map tempMap=(Map)projectList.get(i);
            for(int j=0;j<dictList.size();j++){
                Map codeMap=dictList.get(j);
                String dict_code=codeMap.get("dict_code").toString();
                String item_text=codeMap.get("item_text").toString();
                String item_value=codeMap.get("item_value").toString();
                if(dict_code.equals("projectcatg")&&item_value.equals(tempMap.get("projecttype")==null?null:tempMap.get("projecttype").toString())){
                    tempMap.put("projecttype_dictText",item_text);
                    break;
                }
            }
            projectList2.add(tempMap);
        }
        //政策详情
        List policyList= comPolicyMapper.selectByCreateUser(paramMap);
        List policyList2=new ArrayList();
        for(int i=0;i<policyList.size();i++){
            Map tempMap=(Map)policyList.get(i);
            for(int j=0;j<dictList.size();j++){
                Map codeMap=dictList.get(j);
                String dict_code=codeMap.get("dict_code").toString();
                String item_text=codeMap.get("item_text").toString();
                String item_value=codeMap.get("item_value").toString();

                if(dict_code.equals("policylevel")&&item_value.equals(tempMap.get("level")==null?null:tempMap.get("level").toString())){
                    tempMap.put("level_dictText",item_text);
                    continue;
                }
                if(dict_code.equals("policydept")&&item_value.equals(tempMap.get("deptname")==null?null:tempMap.get("deptname").toString())){
                    tempMap.put("deptname_dictText",item_text);
                    continue;
                }
                if(dict_code.equals("policycatg")&&item_value.equals(tempMap.get("ptype")==null?null:tempMap.get("ptype").toString())){
                    tempMap.put("ptype_dictText",item_text);
                    continue;
                }
                if(dict_code.equals("iscut")&&item_value.equals(tempMap.get("iscut")==null?null:tempMap.get("iscut").toString())){
                    tempMap.put("iscut_dictText",item_text);
                    continue;
                }
            }
            policyList2.add(tempMap);
        }
        //负面清单
        List damagenameList=comDamagingnameMapper.selectByCreateUser(paramMap);
        List damagenameList2=new ArrayList();
        for(int i=0;i<damagenameList.size();i++){
            Map tempMap=(Map)damagenameList.get(i);
            for(int j=0;j<dictList.size();j++){
                Map codeMap=dictList.get(j);
                String dict_code=codeMap.get("dict_code").toString();
                String item_text=codeMap.get("item_text").toString();
                String item_value=codeMap.get("item_value").toString();
                if(dict_code.equals("damagingtype")&&item_value.equals(tempMap.get("damagingtype")==null?null:tempMap.get("damagingtype").toString())){
                    tempMap.put("damagingtype_dictText",item_text);
                    break;
                }
            }
            damagenameList2.add(tempMap);
        }
        //企业人员信息
        List comUserList=comUserMapper.selectByCreateUser(paramMap);
        //经济数据
        List EconomicList=comEconomicMapper.selectByCompanyId(paramMap);
        //投融资信息
        List trzList=comTrzMapper.selectByCompanyId(paramMap);

        resultObject.put("result",true);
        resultObject.put("msg","查询成功");
        resultObject.put("base",new JSONObject(comInfoMap));
        resultObject.put("EconomicList",EconomicList);
        resultObject.put("honorList",honorList2);
        resultObject.put("innovateList",innovateList2);
        resultObject.put("qualList",qualList2);
        resultObject.put("projectList",projectList2);
        resultObject.put("damagenameList",damagenameList2);
        resultObject.put("policyList",policyList2);
        resultObject.put("comUserList",comUserList);
        resultObject.put("trzList",trzList);
        return resultObject;
    }

    /**
     * 查询企业用户
     * @param paramMap
     * @return
     */
    @Override
    public IPage<Map> getComUser(Map paramMap) {
        JSONObject resultObject=new JSONObject();
        IPage<Map> page = new Page<>(Integer.valueOf(paramMap.get("pageNo").toString()), Integer.valueOf(paramMap.get("pageSize").toString()));
        List resultList=comInfoMapper.getComUser(page,paramMap);
        page.setRecords(resultList);
        return page;
    }

    /**
     * 启用
     * @param paramMap
     * @return
     */
    @Override
    public JSONObject startCom(Map paramMap) {
        JSONObject resultObject=new JSONObject();
        Integer result=comInfoMapper.startCom(paramMap);
        resultObject.put("result",true);
        resultObject.put("msg","启用成功");
        return resultObject;
    }
    /**
     * 停用
     * @param paramMap
     * @return
     */
    @Override
    public JSONObject stopCom(Map paramMap) {
        JSONObject resultObject=new JSONObject();
        Integer result=comInfoMapper.stopCom(paramMap);
        resultObject.put("result",true);
        resultObject.put("msg","停用成功");
        return resultObject;
    }

    /**
     * 查询企业列表
     * @return
     */
    @Override
    public JSONObject getComName() {
        JSONObject resultObject=new JSONObject();
        List resultList=comInfoMapper.getComName();
        resultObject.put("result",true);
        resultObject.put("msg","启用成功");
        resultObject.put("list",new JSONArray(resultList));
        return resultObject;
    }

    /**
     * 删除企业-逻辑删除
     * @param idList
     * @return
     */
    @Override
    public JSONObject delByIds(Collection<? extends Serializable> idList) {
        JSONObject resultObject=new JSONObject();
        //删除企业
        comInfoMapper.delByIds(idList);
        //禁用用户
        comInfoMapper.setUserStatus(idList);
        resultObject.put("result",true);
        resultObject.put("msg","删除成功");
        return resultObject;
    }
}
