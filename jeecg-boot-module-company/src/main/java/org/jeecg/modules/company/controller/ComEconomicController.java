package org.jeecg.modules.company.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.company.entity.ComEconomic;
import org.jeecg.modules.company.service.IComEconomicService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.company.service.IComInfoService;
import org.jeecg.modules.company.service.impl.ComInfoServiceImpl;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 经济数据
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
@Api(tags="经济数据")
@RestController
@RequestMapping("/company/comEconomic")
@Slf4j
public class ComEconomicController extends JeecgController<ComEconomic, IComEconomicService> {
	@Autowired
	private IComEconomicService comEconomicService;
	@Autowired
	private IComInfoService comInfoService;

	/**
	 * 分页列表查询
	 *
	 * @param comEconomic
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "经济数据-分页列表查询")
	@ApiOperation(value="经济数据-分页列表查询", notes="经济数据-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ComEconomic comEconomic,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		//查询用户当前的数据
		String token = req.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("查看失败，用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		//判断是否该用户已创建企业信息，如果存在，则执行更新操作
		String id=comInfoService.getByUserName(username);
		comEconomic.setCompanyId(id);
		QueryWrapper<ComEconomic> queryWrapper = QueryGenerator.initQueryWrapper(comEconomic, req.getParameterMap());
		Page<ComEconomic> page = new Page<ComEconomic>(pageNo, pageSize);
		IPage<ComEconomic> pageList = comEconomicService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param comEconomic
	 * @return
	 */
	@AutoLog(value = "经济数据-添加")
	@ApiOperation(value="经济数据-添加", notes="经济数据-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ComEconomic comEconomic,HttpServletRequest req) {
		String token = req.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("查看失败，用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		comEconomic.setCompanyId(id);
		//判断是否有相同年份的
		if(comEconomicService.isYearEconomicExist(comEconomic)){
			return Result.error("已录入该年经济数据！");
		}

		comEconomicService.save(comEconomic);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param comEconomic
	 * @return
	 */
	@AutoLog(value = "经济数据-编辑")
	@ApiOperation(value="经济数据-编辑", notes="经济数据-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ComEconomic comEconomic) {
		comEconomicService.updateById(comEconomic);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "经济数据-通过id删除")
	@ApiOperation(value="经济数据-通过id删除", notes="经济数据-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		comEconomicService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "经济数据-批量删除")
	@ApiOperation(value="经济数据-批量删除", notes="经济数据-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.comEconomicService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "经济数据-通过id查询")
	@ApiOperation(value="经济数据-通过id查询", notes="经济数据-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ComEconomic comEconomic = comEconomicService.getById(id);
		if(comEconomic==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(comEconomic);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param comEconomic
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ComEconomic comEconomic) {
        return super.exportXls(request, comEconomic, ComEconomic.class, "经济数据");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ComEconomic.class);
    }

}
