package org.jeecg.modules.company.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.company.entity.ComDamagingname;
import org.jeecg.modules.company.service.IComDamagingnameService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.company.service.IComInfoService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 负面清单
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Api(tags="负面清单")
@RestController
@RequestMapping("/company/comDamagingname")
@Slf4j
public class ComDamagingnameController extends JeecgController<ComDamagingname, IComDamagingnameService> {
	@Autowired
	private IComDamagingnameService comDamagingnameService;
	 @Autowired
	 private IComInfoService comInfoService;
	/**
	 * 分页列表查询
	 *
	 * @param comDamagingname
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "负面清单-分页列表查询")
	@ApiOperation(value="负面清单-分页列表查询", notes="负面清单-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ComDamagingname comDamagingname,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		String token = req.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("查看失败，用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		if(id !=null){
			comDamagingname.setCompanyId(id);
		}
		QueryWrapper<ComDamagingname> queryWrapper = QueryGenerator.initQueryWrapper(comDamagingname, req.getParameterMap());
		Page<ComDamagingname> page = new Page<ComDamagingname>(pageNo, pageSize);
		IPage<ComDamagingname> pageList = comDamagingnameService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	 /**
	  * 检索接口
	  *
	  * @param pageNo
	  * @param pageSize
	  * @param req
	  * @return
	  */
	 @AutoLog(value = "负面清单-检索")
	 @ApiOperation(value="负面清单-检索", notes="负面清单-检索")
	 @GetMapping(value = "/listByKey")
	 public Result<?> listByKey(	@RequestParam(name="companyId",required = false) String companyId,
	 								@RequestParam(name="company_name",required = false) String company_name,
									@RequestParam(name="damagingtype",required = false) Integer damagingtype,
									@RequestParam(name="industry",required = false) Integer industry,
									@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									HttpServletRequest req) {
		 String token = req.getHeader("X-Access-Token");
		 if(oConvertUtils.isEmpty(token)) {
			 return Result.error("查看失败，用户不存在！");
		 }
		 String username = JwtUtil.getUsername(token);
		 String id=comInfoService.getByUserName(username);

		 JSONObject resultObject=new JSONObject();
	 	 Map paramMap=new HashMap();
		 paramMap.put("company_name",company_name);
		 paramMap.put("damagingtype",damagingtype);
		 paramMap.put("industry",industry);
		 paramMap.put("pageNo",pageNo);
		 paramMap.put("pageSize",pageSize);
		 if(id!=null){
			 paramMap.put("company_id",id);
		 }
		 if(companyId!=null){
			 paramMap.put("company_id",companyId);
		 }
		 IPage<Map> result= comDamagingnameService.listByKey(paramMap);
		 return Result.ok(result);
	 }
	 /**
	  * 关闭清单
	  * @return
	  */
	 @AutoLog(value = "负面清单-关闭")
	 @ApiOperation(value="负面清单-关闭", notes="负面清单-关闭")
	 @GetMapping(value = "/closeComDamagingname")
	 public Result<?> closeComDamagingname(@RequestParam(name="id",required = true) String id) {
		 JSONObject resultObject=new JSONObject();
		 Map paramMap=new HashMap();
		 paramMap.put("id",id);
		 resultObject = comDamagingnameService.closeComDamagingname(paramMap);
		 return Result.ok(resultObject);
	 }
	/**
	 *   添加
	 *
	 * @param comDamagingname
	 * @return
	 */
	@AutoLog(value = "负面清单-添加")
	@ApiOperation(value="负面清单-添加", notes="负面清单-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ComDamagingname comDamagingname) {
		comDamagingnameService.save(comDamagingname);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param comDamagingname
	 * @return
	 */
	@AutoLog(value = "负面清单-编辑")
	@ApiOperation(value="负面清单-编辑", notes="负面清单-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ComDamagingname comDamagingname) {
		comDamagingnameService.updateById(comDamagingname);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "负面清单-通过id删除")
	@ApiOperation(value="负面清单-通过id删除", notes="负面清单-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		comDamagingnameService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "负面清单-批量删除")
	@ApiOperation(value="负面清单-批量删除", notes="负面清单-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.comDamagingnameService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "负面清单-通过id查询")
	@ApiOperation(value="负面清单-通过id查询", notes="负面清单-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ComDamagingname comDamagingname = comDamagingnameService.getById(id);
		if(comDamagingname==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(comDamagingname);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param comDamagingname
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ComDamagingname comDamagingname) {
        return super.exportXls(request, comDamagingname, ComDamagingname.class, "负面清单");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ComDamagingname.class);
    }

}
