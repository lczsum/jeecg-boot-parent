package org.jeecg.modules.company.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.entity.ComProgress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 办理进度
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface ComProgressMapper extends BaseMapper<ComProgress> {

    /**
     * 关键字检索
     * @param paramMap
     * @return
     */
    public List listByKey(IPage<Map> page,@Param("map")Map paramMap);
    /**
     * 办理进度
     * @param page
     * @param paramMap
     * @return
     */
    public List adminListByKey(IPage<Map> page, @Param("map") Map paramMap);

}
