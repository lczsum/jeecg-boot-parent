package org.jeecg.modules.company.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.company.entity.ComTrz;
import org.jeecg.modules.company.service.IComInfoService;
import org.jeecg.modules.company.service.IComTrzService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 投融资需求
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Api(tags="投融资需求")
@RestController
@RequestMapping("/company/comTrz")
@Slf4j
public class ComTrzController extends JeecgController<ComTrz, IComTrzService> {
	@Autowired
	private IComTrzService comTrzService;
	 @Autowired
	 private IComInfoService comInfoService;

	/**
	 * 分页列表查询
	 *
	 * @param comTrz
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "投融资需求-分页列表查询")
	@ApiOperation(value="投融资需求-分页列表查询", notes="投融资需求-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ComTrz comTrz,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		String token = req.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("查看失败，用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		comTrz.setCompanyId(id);
		QueryWrapper<ComTrz> queryWrapper = QueryGenerator.initQueryWrapper(comTrz, req.getParameterMap());
		Page<ComTrz> page = new Page<ComTrz>(pageNo, pageSize);
		IPage<ComTrz> pageList = comTrzService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	 /**
	  * 检索
	  * @param pageNo
	  * @param pageSize
	  * @param req
	  * @return
	  */
	 @AutoLog(value = "投融资需求-分页列表查询")
	 @ApiOperation(value="投融资需求-分页列表查询", notes="投融资需求-分页列表查询")
	 @GetMapping(value = "/getTrzByKey")
	 public Result<?> getTrzByKey(@RequestParam(name="trzname",required = false) String trzname,
									@RequestParam(name="amount",required = false) String amount,
								  @RequestParam(name="pageNo",defaultValue = "1") Integer pageNo,
									@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									HttpServletRequest req) {
		 String token = req.getHeader("X-Access-Token");
		 if(oConvertUtils.isEmpty(token)) {
			 return Result.error("查看失败，用户不存在！");
		 }
		 String username = JwtUtil.getUsername(token);
		 String id=comInfoService.getByUserName(username);

	 	Map paramMap=new HashMap();
		 paramMap.put("trzname",trzname);
		 paramMap.put("amount",amount);
		 paramMap.put("pageNo",pageNo);
		 paramMap.put("pageSize",pageSize);
		 paramMap.put("username",username);
		 paramMap.put("company_id",id);
		 IPage<Map> pageList =comTrzService.getTrzByKey(paramMap);
		 return Result.ok(pageList);
	 }
	/**
	 *   添加
	 *
	 * @param comTrz
	 * @return
	 */
	@AutoLog(value = "投融资需求-添加")
	@ApiOperation(value="投融资需求-添加", notes="投融资需求-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ComTrz comTrz,HttpServletRequest req) {
		String token = req.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("查看失败，用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		comTrz.setCompanyId(id);
		comTrzService.save(comTrz);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param comTrz
	 * @return
	 */
	@AutoLog(value = "投融资需求-编辑")
	@ApiOperation(value="投融资需求-编辑", notes="投融资需求-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ComTrz comTrz) {
		comTrzService.updateById(comTrz);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "投融资需求-通过id删除")
	@ApiOperation(value="投融资需求-通过id删除", notes="投融资需求-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		comTrzService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "投融资需求-批量删除")
	@ApiOperation(value="投融资需求-批量删除", notes="投融资需求-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.comTrzService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "投融资需求-通过id查询")
	@ApiOperation(value="投融资需求-通过id查询", notes="投融资需求-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ComTrz comTrz = comTrzService.getById(id);
		if(comTrz==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(comTrz);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param comTrz
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ComTrz comTrz) {
        return super.exportXls(request, comTrz, ComTrz.class, "投融资需求");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ComTrz.class);
    }

}
