package org.jeecg.modules.company.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.company.entity.ComTrz;
import org.jeecg.modules.company.mapper.ComTrzMapper;
import org.jeecg.modules.company.service.IComTrzService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Description: 投融资需求
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Service
public class ComTrzServiceImpl extends ServiceImpl<ComTrzMapper, ComTrz> implements IComTrzService {
    @Resource
    private ComTrzMapper comTrzMapper;
    /**
     * 投融资检索
     * @param paramMap
     * @return
     */
    @Override
    public IPage<Map> getTrzByKey(Map paramMap) {
        IPage<Map> page = new Page<>(Integer.valueOf(paramMap.get("pageNo").toString()), Integer.valueOf(paramMap.get("pageSize").toString()));
        List resultList= comTrzMapper.getTrzByKey(page,paramMap);
        page.setRecords(resultList);
        return page;
    }
}
