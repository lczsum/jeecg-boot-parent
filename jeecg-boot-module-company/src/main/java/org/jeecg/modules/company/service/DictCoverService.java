package org.jeecg.modules.company.service;

import java.util.List;
import java.util.Map;

/**
 * 数据字典处理
 */
public interface DictCoverService {
    /**
     * 根据类型和value值获取查询结果
     * @param codes
     * @return
     */
    public List<Map> getDictCode(List codes);

}
