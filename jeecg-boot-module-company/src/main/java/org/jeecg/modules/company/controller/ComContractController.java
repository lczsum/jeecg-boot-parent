package org.jeecg.modules.company.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.company.entity.ComContract;
import org.jeecg.modules.company.service.IComContractService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.company.service.IComInfoService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 投资合同
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Api(tags="投资合同")
@RestController
@RequestMapping("/company/comContract")
@Slf4j
public class ComContractController extends JeecgController<ComContract, IComContractService> {
	@Autowired
	private IComContractService comContractService;
	 @Autowired
	 private IComInfoService comInfoService;
	/**
	 * 分页列表查询
	 *
	 * @param comContract
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "投资合同-分页列表查询")
	@ApiOperation(value="投资合同-分页列表查询", notes="投资合同-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ComContract comContract,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {

		String token = req.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("查看失败，用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		comContract.setCompanyId(id);
		QueryWrapper<ComContract> queryWrapper = QueryGenerator.initQueryWrapper(comContract, req.getParameterMap());
		Page<ComContract> page = new Page<ComContract>(pageNo, pageSize);
		IPage<ComContract> pageList = comContractService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param comContract
	 * @return
	 */
	@AutoLog(value = "投资合同-添加")
	@ApiOperation(value="投资合同-添加", notes="投资合同-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ComContract comContract,HttpServletRequest request) {
		String token = request.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("查看失败，用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		comContract.setCompanyId(id);
		comContractService.save(comContract);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param comContract
	 * @return
	 */
	@AutoLog(value = "投资合同-编辑")
	@ApiOperation(value="投资合同-编辑", notes="投资合同-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ComContract comContract) {
		comContractService.updateById(comContract);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "投资合同-通过id删除")
	@ApiOperation(value="投资合同-通过id删除", notes="投资合同-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		comContractService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "投资合同-批量删除")
	@ApiOperation(value="投资合同-批量删除", notes="投资合同-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.comContractService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "投资合同-通过id查询")
	@ApiOperation(value="投资合同-通过id查询", notes="投资合同-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ComContract comContract = comContractService.getById(id);
		if(comContract==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(comContract);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param comContract
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ComContract comContract) {
        return super.exportXls(request, comContract, ComContract.class, "投资合同");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ComContract.class);
    }

}
