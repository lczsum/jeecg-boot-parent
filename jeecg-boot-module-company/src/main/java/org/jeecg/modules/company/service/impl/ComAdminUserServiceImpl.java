package org.jeecg.modules.company.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.PasswordUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.company.dto.ComAdminUserDto;
import org.jeecg.modules.company.mapper.ComAdminUserMapper;
import org.jeecg.modules.company.service.IComAdminUserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service
public class ComAdminUserServiceImpl extends ServiceImpl<ComAdminUserMapper, ComAdminUserDto>  implements IComAdminUserService{

	@Resource
	private ComAdminUserMapper comAdminUserMapper;
	@Value("${company.role-code}")
	private String companyRoleCode;

	@Override
	public Page<ComAdminUserDto> page(ComAdminUserDto comAdminUserDto,Page<ComAdminUserDto> page) {
		//获取用户当前登录用户信息
		LoginUser sysUser = (LoginUser)SecurityUtils.getSubject().getPrincipal();
		//获取当前登录用户的企业信息。
		ComAdminUserDto company = comAdminUserMapper.getUserCompanyByUserId(sysUser.getId());
		if(company == null || StringUtils.isBlank(company.getCompanyId())) {
			return page;
		}
		comAdminUserDto.setCompanyId(company.getCompanyId());
		return page.setRecords(comAdminUserMapper.queryComAdminUserPage(page, comAdminUserDto));
	}

	@Override
	public boolean save(ComAdminUserDto user)  {
		try {
			user.setId(UUID.randomUUID().toString().replaceAll("-",""));
			user.setUserId(UUID.randomUUID().toString().replaceAll("-",""));
			user.setCreateTime(new Date());//设置创建时间
			String salt = oConvertUtils.randomGen(8);
			user.setSalt(salt);
			String passwordEncode = PasswordUtil.encrypt(user.getUsername(), user.getPassword(), salt);
			user.setPassword(passwordEncode);
			user.setStatus(1);
			user.setDelFlag("0");
			//保存用户
			comAdminUserMapper.insertSysUser(user);
			//获取用户当前登录用户信息
			LoginUser sysUser = (LoginUser)SecurityUtils.getSubject().getPrincipal();
			//ComAdminUserDto  userdto = comAdminUserMapper.getUserByUsername(user.getCreateBy());
			//保存用户企业信息
			//获取当前登录用户的企业信息。
			ComAdminUserDto company = comAdminUserMapper.getUserCompanyByUserId(sysUser.getId());
			if(company!=null && StringUtils.isNotBlank(company.getCompanyId() )) {
				user.setCompanyId(company.getCompanyId());
				comAdminUserMapper.addUserWithCompany(user);
			}
			//保存用户角色关系
			//获取当前登录用户的角色信息。
			List<String> rList=  comAdminUserMapper.getUserRoleByUserId(sysUser.getId());
			for (String string : rList) {
				user.setRoleId(string);
				comAdminUserMapper.addUserWithRole(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteUser(String id) {
		comAdminUserMapper.deleteUser(id);
	}

	@Override
	public void deleteBatchUsers(String ids) {
		//1.删除用户
		comAdminUserMapper.removeByIds(Arrays.asList(ids.split(",")));
	}

	@Override
	public void frozenBatch(List<String> list, String status) {
		ComAdminUserDto  dto =new ComAdminUserDto();
		dto.setUserIds(list);
		dto.setStatus(Integer.valueOf(status));
		comAdminUserMapper.frozenBatch(dto);
	}

	@Override
	public List<ComAdminUserDto> findList(ComAdminUserDto dto) {
		return comAdminUserMapper.findList(dto);
	}

	@Override
	public void changePassword(ComAdminUserDto dto) {
		String salt = oConvertUtils.randomGen(8);
		dto.setSalt(salt);
		String passwordEncode = PasswordUtil.encrypt(dto.getUsername(), dto.getPassword(), salt);
		dto.setPassword(passwordEncode);
		comAdminUserMapper.changePassword(dto);
	}

	@Override
	public void edit(ComAdminUserDto dto) {
		if(StringUtils.isNotBlank(dto.getPassword())) {
			String salt = oConvertUtils.randomGen(8);
			dto.setSalt(salt);
			String passwordEncode = PasswordUtil.encrypt(dto.getUsername(), dto.getPassword(), salt);
			dto.setPassword(passwordEncode);
    	}
		comAdminUserMapper.edit(dto);
	}

	@Override
	public void saveCompanyAdminUser(ComAdminUserDto dto) {
		dto.setUsername(dto.getRealname().trim());
		dto.setPassword("123456");
		dto.setId(UUID.randomUUID().toString().replaceAll("-",""));
		dto.setUserId(UUID.randomUUID().toString().replaceAll("-",""));
		dto.setCreateTime(new Date());//设置创建时间
		String salt = oConvertUtils.randomGen(8);
		dto.setSalt(salt);
		String passwordEncode = PasswordUtil.encrypt(dto.getUsername().trim(), dto.getPassword(), salt);
		dto.setPassword(passwordEncode);
		dto.setStatus(1);
		dto.setDelFlag("0");
		//保存用户
		comAdminUserMapper.insertSysUser(dto);
		//保存用户角色关系
		//获取当前登录用户的角色信息。
		//dto.setRoleId("1238743913318105090");//可将角色编号写入配置文件中
		dto.setRoleId(companyRoleCode);
		comAdminUserMapper.addUserWithRole(dto);
		comAdminUserMapper.addUserWithCompany(dto);
	}

	@Override
	public ComAdminUserDto getUserCompanyByUserId() {
		LoginUser sysUser = (LoginUser)SecurityUtils.getSubject().getPrincipal();
		//保存用户企业信息
		//获取当前登录用户的企业信息。
		ComAdminUserDto company = comAdminUserMapper.getUserCompanyByUserId(sysUser.getId());
		return company;
	}

}
