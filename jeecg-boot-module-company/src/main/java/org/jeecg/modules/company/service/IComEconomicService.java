package org.jeecg.modules.company.service;

import org.jeecg.modules.company.entity.ComEconomic;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 经济数据
 * @Author: jeecg-boot
 * @Date:   2020-04-04
 * @Version: V1.0
 */
public interface IComEconomicService extends IService<ComEconomic> {
    /**
     * 判断是否已录入该年的经济数据
     * @param comEconomic
     * @return
     */
    Boolean isYearEconomicExist(ComEconomic comEconomic);

}
