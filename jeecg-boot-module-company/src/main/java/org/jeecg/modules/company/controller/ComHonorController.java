package org.jeecg.modules.company.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.company.entity.ComHonor;
import org.jeecg.modules.company.service.IComHonorService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.company.service.IComInfoService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 品牌荣誉
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Api(tags="品牌荣誉")
@RestController
@RequestMapping("/company/comHonor")
@Slf4j
public class ComHonorController extends JeecgController<ComHonor, IComHonorService> {
	@Autowired
	private IComHonorService comHonorService;
	 @Autowired
	 private IComInfoService comInfoService;

	/**
	 * 分页列表查询
	 *
	 * @param comHonor
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "品牌荣誉-分页列表查询")
	@ApiOperation(value="品牌荣誉-分页列表查询", notes="品牌荣誉-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ComHonor comHonor,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		String token = req.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("查看失败，用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		comHonor.setCompanyId(id);
		QueryWrapper<ComHonor> queryWrapper = QueryGenerator.initQueryWrapper(comHonor, req.getParameterMap());
		Page<ComHonor> page = new Page<ComHonor>(pageNo, pageSize);
		IPage<ComHonor> pageList = comHonorService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param comHonor
	 * @return
	 */
	@AutoLog(value = "品牌荣誉-添加")
	@ApiOperation(value="品牌荣誉-添加", notes="品牌荣誉-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ComHonor comHonor,HttpServletRequest request) {
		String token = request.getHeader("X-Access-Token");
		if(oConvertUtils.isEmpty(token)) {
			return Result.error("查看失败，用户不存在！");
		}
		String username = JwtUtil.getUsername(token);
		String id=comInfoService.getByUserName(username);
		comHonor.setCompanyId(id);
		comHonorService.save(comHonor);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param comHonor
	 * @return
	 */
	@AutoLog(value = "品牌荣誉-编辑")
	@ApiOperation(value="品牌荣誉-编辑", notes="品牌荣誉-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ComHonor comHonor) {
		comHonorService.updateById(comHonor);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "品牌荣誉-通过id删除")
	@ApiOperation(value="品牌荣誉-通过id删除", notes="品牌荣誉-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		comHonorService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "品牌荣誉-批量删除")
	@ApiOperation(value="品牌荣誉-批量删除", notes="品牌荣誉-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.comHonorService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "品牌荣誉-通过id查询")
	@ApiOperation(value="品牌荣誉-通过id查询", notes="品牌荣誉-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ComHonor comHonor = comHonorService.getById(id);
		if(comHonor==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(comHonor);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param comHonor
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ComHonor comHonor) {
        return super.exportXls(request, comHonor, ComHonor.class, "品牌荣誉");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ComHonor.class);
    }

}
