package org.jeecg.modules.company.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.company.entity.ComPolicy;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * @Description: 政策扶持
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface IComPolicyService extends IService<ComPolicy> {
    /**
     * 获取用户自身扶持政策
     * @param paramMap
     * @return
     */
    public IPage<Map> getComOwnList(Map paramMap);

    /**
     * 查询查询企业政策扶持
     * @param page
     * @param comPolicy
     * @return
     */
    public IPage<Map> listComPolicy(Page<Map> page,ComPolicy comPolicy);
}
