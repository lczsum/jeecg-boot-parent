package org.jeecg.modules.company.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.entity.ComColdchain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 冷链物流
 * @Author: jeecg-boot
 * @Date:   2020-04-25
 * @Version: V1.0
 */
public interface ComColdchainMapper extends BaseMapper<ComColdchain> {

}
