package org.jeecg.modules.company.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.company.entity.ComUser;
import org.jeecg.modules.company.mapper.ComUserMapper;
import org.jeecg.modules.company.service.IComUserService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Description: 企业人员
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Service
public class ComUserServiceImpl extends ServiceImpl<ComUserMapper, ComUser> implements IComUserService {
    @Resource
    private ComUserMapper comUserMapper;
    /**
     * 检索企业员工
     * @param pageNo
     * @param pageSize
     * @param comUser
     * @return
     */
    @Override
    public IPage<ComUser> listByKey(Integer pageNo, Integer pageSize, ComUser comUser) {
        IPage<ComUser> page = new Page<>(pageNo, pageSize);
        List<ComUser> resultList=comUserMapper.listByKey(page,comUser);

        page.setRecords(resultList);
        return page;

    }

    /**
     * 查询省
     * @return
     */
    @Override
    public List getProvince() {
        List resultList=comUserMapper.getProvince();
        return resultList;
    }

    /**
     * 根据省id查询城市
     * @param paramMap
     * @return
     */
    @Override
    public List getCityByPid(Map paramMap) {
        List resultList=comUserMapper.getCityByPid(paramMap);
        return resultList;
    }

    /**
     *校验企业法人
     * @param paramMap
     * @return
     */
    @Override
    public boolean checkUnique(Map paramMap) {
        List<ComUser> com=comUserMapper.checkUnique(paramMap);
        if(com.isEmpty()){
            return false;
        }else{
            return true;
        }
    }
}
