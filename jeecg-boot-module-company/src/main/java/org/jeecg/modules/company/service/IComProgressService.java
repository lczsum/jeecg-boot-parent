package org.jeecg.modules.company.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.jeecg.modules.company.entity.ComProgress;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Description: 办理进度
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface IComProgressService extends IService<ComProgress> {
    /**
     * 企业查询自己得办理进度
     * @param paramMap
     * @return
     */
    public IPage<Map> listByKey(Map paramMap);

    /**
     * 管理员查看所有办理进度
     * @param paramMap
     * @return
     */
    public IPage<Map> adminListByKey(Map paramMap);

}
