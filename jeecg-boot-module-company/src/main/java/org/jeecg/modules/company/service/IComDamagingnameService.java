package org.jeecg.modules.company.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.jeecg.modules.company.entity.ComDamagingname;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * @Description: 负面清单
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface IComDamagingnameService extends IService<ComDamagingname> {
    /**
     * 负面清单检索接口
     * @return
     */
    public IPage<Map> listByKey(Map paramMap);

    /**
     * 关闭负面清单
     * @param paramMap
     * @return
     */
    public JSONObject closeComDamagingname(Map paramMap);

}
