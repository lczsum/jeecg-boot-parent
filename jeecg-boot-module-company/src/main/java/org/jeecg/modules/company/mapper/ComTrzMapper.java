package org.jeecg.modules.company.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.company.entity.ComTrz;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 投融资需求
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface ComTrzMapper extends BaseMapper<ComTrz> {

    /**
     * 投融资检索
     * @param paramMap
     * @return
     */
    List getTrzByKey(IPage<Map> page,@Param("map") Map paramMap);

    /**
     * 查询投融资信息
     * @return
     */
    List selectByCompanyId(Map paramMap);
}
