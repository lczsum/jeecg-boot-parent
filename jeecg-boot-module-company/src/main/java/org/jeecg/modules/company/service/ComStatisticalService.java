package org.jeecg.modules.company.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 统计类
 */

public interface ComStatisticalService {
    /**
     *获取首页统计
     * @return
     */
    public JSONObject getIndexStatustical();

    /**
     * 数据统计分析
     * @return
     */
    public JSONObject getDataStatustical(Map paramMap);
}
