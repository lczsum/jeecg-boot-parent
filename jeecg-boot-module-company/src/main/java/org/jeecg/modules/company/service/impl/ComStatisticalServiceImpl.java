package org.jeecg.modules.company.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.hibernate.validator.internal.util.StringHelper;
import org.jeecg.modules.company.mapper.ComStatisticalServiceMapper;
import org.jeecg.modules.company.service.ComStatisticalService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 统计类
 */
@Service
public class ComStatisticalServiceImpl implements ComStatisticalService {


    @Resource
    private ComStatisticalServiceMapper comStatisticalServiceMapper;

    /**
     *获取首页统计
     * @return
     */

    @Override
    public JSONObject getIndexStatustical() {
        //Map resultMap=comStatisticalServiceMapper.getIndexStatustical();
        JSONObject resultObject=new JSONObject();
        //1企业数
        Integer comCount=comStatisticalServiceMapper.getComCount();
        resultObject.put("comCount",comCount);
        //2行业类比占比
        List industryRatioList=comStatisticalServiceMapper.getIndustryRatio();
        resultObject.put("industryRatioList",industryRatioList);
        //3企业数量规模统计饼图
        List comScaleRatioList=comStatisticalServiceMapper.getComScaleRatio();
        resultObject.put("comScaleRatioList",comScaleRatioList);
        //4负面企业走势图
        List damageLineChartsList=comStatisticalServiceMapper.getDamageLineCharts();
        resultObject.put("damageLineChartsList",damageLineChartsList);
        //5负面类型饼图
        List damagePiesList=comStatisticalServiceMapper.getDamagePies();
        resultObject.put("damagePiesList",damagePiesList);
        //6创新走势图
        List invateLineChartsList=comStatisticalServiceMapper.getInnvateLineCharts();
        resultObject.put("invateLineChartsList",invateLineChartsList);
        //7创新类别饼图
        /*List innvatePiesList=comStatisticalServiceMapper.getInnvatePies();
        resultObject.put("innvatePiesList",innvatePiesList);*/
        //8园区投融资统走势图
        List trzLineChartsList=comStatisticalServiceMapper.getTrzLineCharts();
        resultObject.put("trzLineChartsList",trzLineChartsList);
        //9投融资金额企业饼图（10万，50万，100万，500万，1000万，5000万，1亿）
        List trzPiesList=comStatisticalServiceMapper.getTrzPies();
        resultObject.put("trzPiesList",trzPiesList);
        //10品牌荣誉饼图
        List honorPiesList=comStatisticalServiceMapper.getHonorPies();
        resultObject.put("honorPiesList",honorPiesList);
        //11资质认证饼图
        List qualPiesList=comStatisticalServiceMapper.getQualPies();
        resultObject.put("qualPiesList",qualPiesList);
        //12统计总产值、总税收、总固投
        Map allEcnomicData=comStatisticalServiceMapper.getAllEcnomic();
        //总产值
        resultObject.put("outputvalue",allEcnomicData.get("outputvalue")==null?0:allEcnomicData.get("outputvalue").toString());
        //总税收
        resultObject.put("tax",allEcnomicData.get("tax")==null?0:allEcnomicData.get("tax").toString());
        //总固投
        resultObject.put("solidinvestment",allEcnomicData.get("solidinvestment")==null?0:allEcnomicData.get("solidinvestment").toString());
        //13户籍饼图
        Map paramMap=new HashMap();
        List provinceCityList= comStatisticalServiceMapper.getProvinceCityCount(paramMap);
        resultObject.put("provinceCityList",provinceCityList);
        //14人员统计学历饼图
        List<Map> usrPiesList= comStatisticalServiceMapper.getUsrPies(paramMap);
        resultObject.put("usrPiesList",usrPiesList);
        //15年龄饼图
        List<Map> usrAgePiesList= comStatisticalServiceMapper.getUsrAgePiesList(paramMap);
        resultObject.put("usrAgePiesList",usrAgePiesList);
        //16性别饼图
        List<Map> usrSexPiesList= comStatisticalServiceMapper.getUsrSexPiesList(paramMap);
        resultObject.put("usrSexPiesList",usrSexPiesList);
        //17户籍柱状图
        List<Map> usrCityPiesList= comStatisticalServiceMapper.getUsrCityPiesList(paramMap);
        resultObject.put("usrCityPiesList",usrCityPiesList);
        //18企业费用统计
        List<Map> costList=comStatisticalServiceMapper.getCostMap();
        resultObject.put("costList",costList);

        resultObject.put("result",true);
        resultObject.put("msg","查询成功");
        return resultObject;
    }

    /**
     * 数据统计分析
     * @param paramMap
     * @return
     */
    @Override
    public JSONObject getDataStatustical(Map paramMap) {
        JSONObject resultObject=new JSONObject();
        if(!paramMap.isEmpty()&&paramMap.get("type")!=null){
            if("tax".equals(paramMap.get("type").toString())){
                //税收
                //查询数据
                if(paramMap.get("lower_tax")!=null&& !StringHelper.isNullOrEmptyString(paramMap.get("lower_tax").toString())){
                    paramMap.put("lower_tax",Double.valueOf(paramMap.get("lower_tax").toString()));
                }
                if(paramMap.get("up_tax")!=null&& !StringHelper.isNullOrEmptyString(paramMap.get("up_tax").toString())){
                    paramMap.put("up_tax",Double.valueOf(paramMap.get("up_tax").toString()));
                }
                List<Map> taxDataList= comStatisticalServiceMapper.getTaxData(paramMap);
                resultObject.put("taxDataList",taxDataList);
                //统计趋势图
                List<Map> taxLineChartsList= comStatisticalServiceMapper.getTaxLineChartsList(paramMap);
                resultObject.put("taxLineChartsList",taxLineChartsList);
                //如果检索条件有行业，则显示行业税收占比，如果没有筛选行业，则显示行业柱状图
                if(paramMap.get("industry")!=null){
                    Map map= comStatisticalServiceMapper.getTaxIndustryRatio(paramMap);
                    if(map!=null){
                        if(map.get("thisindustry")==null){
                            resultObject.put("taxIndustryRatio",new Double(0));
                        }else{
                            Double thisindustry=Double.valueOf(map.get("thisindustry").toString());
                            Double otherindustry=Double.valueOf(map.get("otherindustry").toString());
                            resultObject.put("taxIndustryRatio",thisindustry/otherindustry);
                        }
                    }else{
                        resultObject.put("taxIndustryRatio",0);
                    }

                }else{
                //统计行业柱状图
                List<Map> taxBarList= comStatisticalServiceMapper.getTaxBarList(paramMap);
                    resultObject.put("taxBarList",taxBarList);
                }
            }else if("opv".equals(paramMap.get("type").toString())){
                //产值
                if(paramMap.get("lower_opv")!=null&&!StringHelper.isNullOrEmptyString(paramMap.get("lower_opv").toString())){
                    paramMap.put("lower_opv",Double.valueOf(paramMap.get("lower_opv").toString()));
                }
                if(paramMap.get("up_opv")!=null&&!StringHelper.isNullOrEmptyString(paramMap.get("up_opv").toString())){
                    paramMap.put("up_opv",Double.valueOf(paramMap.get("up_opv").toString()));
                }
                //查询数据
                List<Map> OpvDataList= comStatisticalServiceMapper.getOpvData(paramMap);
                resultObject.put("OpvDataList",OpvDataList);
                //统计趋势图
                List<Map> OpvLineChartsList= comStatisticalServiceMapper.getOpvLineChartsList(paramMap);
                resultObject.put("OpvLineChartsList",OpvLineChartsList);
                //如果检索条件有行业，则显示行业税收占比，如果没有筛选行业，则显示行业柱状图
                if(paramMap.get("industry")!=null){
                    Map map= comStatisticalServiceMapper.getOpvIndustryRatio(paramMap);
                    if(map!=null){
                        if(map.get("thisindustry")==null){
                            resultObject.put("opvIndustryRatio",new Double(0));
                        }else{
                            Double thisindustry=Double.valueOf(map.get("thisindustry").toString());
                            Double otherindustry=Double.valueOf(map.get("otherindustry").toString());
                            resultObject.put("opvIndustryRatio",thisindustry/otherindustry);
                        }
                    }else{
                        resultObject.put("opvIndustryRatio",0);
                    }

                }else{
                    //统计行业柱状图
                    List<Map> opvBarList= comStatisticalServiceMapper.getOpvBarList(paramMap);
                    resultObject.put("opvBarList",opvBarList);
                }

            }else if("usr".equals(paramMap.get("type").toString())){
                //人员

                //查询数据
                List<Map> usrDataList= comStatisticalServiceMapper.getUsrData(paramMap);
                resultObject.put("usrDataList",usrDataList);
                //统计学历饼图
                List<Map> usrPiesList= comStatisticalServiceMapper.getUsrPies(paramMap);
                resultObject.put("usrPiesList",usrPiesList);
               //年龄学历分布图
                List<Map> usrScatterList= comStatisticalServiceMapper.getUsrScatterList(paramMap);
                List userScatterList2=new ArrayList();
                for(Map map:usrScatterList){
                    if(map!=null){
                        List tempList=new ArrayList();
                        tempList.add(map.get("age"));
                        tempList.add(map.get("education"));
                        userScatterList2.add(tempList);
                    }
                }
                resultObject.put("usrScatterList",userScatterList2);
                //统计年龄饼图
                List<Map> usrAgePiesList= comStatisticalServiceMapper.getUsrAgePiesList(paramMap);
                resultObject.put("usrAgePiesList",usrAgePiesList);
                //性别饼图
                List<Map> usrSexPiesList= comStatisticalServiceMapper.getUsrSexPiesList(paramMap);
                resultObject.put("usrSexPiesList",usrSexPiesList);
                //户籍饼图
                List provinceCityList= comStatisticalServiceMapper.getProvinceCityCount(paramMap);
                resultObject.put("provinceCityList",provinceCityList);
                //户籍柱状图
                List<Map> usrCityPiesList= comStatisticalServiceMapper.getUsrCityPiesList(paramMap);
                resultObject.put("usrCityPiesList",usrCityPiesList);
                //技术技能、经营管理、营销、高层次占比
                List<Map> usrTypePiesList= comStatisticalServiceMapper.getUserTypePiesList(paramMap);
                resultObject.put("usrTypePiesList",usrTypePiesList);
            }
            resultObject.put("result",true);
            resultObject.put("msg","查询成功");
        }else{
            resultObject.put("result",false);
            resultObject.put("msg","查询失败，请提供类型参数！");
        }
        return resultObject;
    }
}
