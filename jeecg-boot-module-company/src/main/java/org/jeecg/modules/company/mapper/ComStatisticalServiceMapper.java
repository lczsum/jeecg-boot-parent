package org.jeecg.modules.company.mapper;

import java.util.List;
import java.util.Map;

/**
 * 统计类
 */
public interface ComStatisticalServiceMapper {
    /**
     * 查询
     * @return
     */
    public Map getIndexStatustical();

    /**
     * 企业数量统计
     * @return
     */
    public Integer getComCount();

    /**
     * 行业类别占比
     * @return
     */
    public List getIndustryRatio();

    /**
     * 企业数量规模统计
     * @return
     */
    public List getComScaleRatio();

    /**
     * 负面企业走势图
     * @return
     */
    public List getDamageLineCharts();

    /**
     * 获取负面类型统计
     * @return
     */
    public List getDamagePies();

    /**
     * 创新类型走势图
     * @return
     */
    public List getInnvateLineCharts();

    /**
     * 创新类型统计饼图
     * @return
     */
    public List getInnvatePies();

    /**
     * 获取投融资走势图
     * @return
     */
    public List getTrzLineCharts();

    /**
     * 获取投融资饼图
     * @return
     */
    public List getTrzPies();

    /**
     * 获取荣誉统计
     * @return
     */
    public List getHonorPies();

    /**
     * 获取资质统计
     * @return
     */
    public List getQualPies();

    /**
     * 查询税收
     * @return
     */
    public List<Map> getTaxData(Map param);

    /**
     * 查询税收趋势图
     * @return
     */
    public List<Map> getTaxLineChartsList(Map param);

    /**
     *查询行业税收占比
     * @return
     */
    public Map getTaxIndustryRatio(Map paramMap);

    /**
     * 查询行业税收柱状图
     * @param paramMap
     * @return
     */
    public List getTaxBarList(Map paramMap);

    /**
     * 查询产值
     * @param param
     * @return
     */
    public List<Map> getOpvData(Map param);
    /**
     * 查询产值趋势图
     * @return
     */
    public List<Map> getOpvLineChartsList(Map param);
    /**
     *查询行业产值占比
     * @return
     */
    public Map getOpvIndustryRatio(Map paramMap);
    /**
     * 查询行业产值柱状图
     * @param paramMap
     * @return
     */
    public List getOpvBarList(Map paramMap);

    /**
     * 查询员工
     * @param paramMap
     * @return
     */
    public List<Map> getUsrData(Map paramMap);

    /**
     * 查询用户学历饼图
     * @param paramMap
     * @return
     */
    public List getUsrPies(Map paramMap);

    /**
     * 年龄学历散点图
     * @param paramMap
     * @return
     */
    public List getUsrScatterList(Map paramMap);

    /**
     * 统计总产值、总税收、总固投
     * @param
     * @return
     */
    public Map getAllEcnomic();

    /**
     * 查询户籍饼图
     * @return
     */
    public List getProvinceCityCount(Map paramMap);

    /**
     * 统计年龄饼图
     * @return
     */
    public List getUsrAgePiesList(Map  paramMap);

    /**
     * 性别统计饼图
     * @param paramMap
     * @return
     */
    public List getUsrSexPiesList(Map paramMap);

    /**
     * 户籍柱状图
     * @param paramMap
     * @return
     */
    public List getUsrCityPiesList(Map paramMap);

    /**
     * 企业费用统计
     * @return
     */
    public List<Map> getCostMap();

    /**
     * 统计技术技能、经营管理、营销、高层次占比
     * @return
     */
    public List<Map> getUserTypePiesList(Map paramMap);
}
