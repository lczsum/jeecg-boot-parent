package org.jeecg.modules.company.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.company.entity.ComProgress;
import org.jeecg.modules.company.service.IComInfoService;
import org.jeecg.modules.company.service.IComProgressService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 办理进度
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Api(tags="办理进度")
@RestController
@RequestMapping("/company/comProgress")
@Slf4j
public class ComProgressController extends JeecgController<ComProgress, IComProgressService> {
	@Autowired
	private IComProgressService comProgressService;
	 @Autowired
	 private IComInfoService comInfoService;
	/**
	 * 分页列表查询
	 *
	 * @param
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "办理进度-分页列表查询")
	@ApiOperation(value="办理进度-分页列表查询", notes="办理进度-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(@RequestParam(name="companyId",required = false) String  companyId,
									@RequestParam(name="progress",required = false) Integer  progress,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
	/*	QueryWrapper<ComProgress> queryWrapper = QueryGenerator.initQueryWrapper(comProgress, req.getParameterMap());
		Page<ComProgress> page = new Page<ComProgress>(pageNo, pageSize);
		IPage<ComProgress> pageList = comProgressService.page(page, queryWrapper);
*/
		Map paramMap=new HashMap();
		paramMap.put("progress",progress);
		paramMap.put("companyId",companyId);
		paramMap.put("pageNo",pageNo);
		paramMap.put("pageSize",pageSize);
		IPage<Map> result=comProgressService.adminListByKey(paramMap);
		return Result.ok(result);
	}

	 /**
	  * 分页列表检索
	  *
	  * @param
	  * @param pageNo
	  * @param pageSize
	  * @param req
	  * @return
	  */
	 @AutoLog(value = "办理进度-分页列表查询")
	 @ApiOperation(value="办理进度-分页列表查询", notes="办理进度-分页列表查询")
	 @GetMapping(value = "/listByKey")
	 public Result<?> listByKey(
								@RequestParam(name="progress",required = false) Integer  progress,
								@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									HttpServletRequest req) {
		 JSONObject resultObject=new JSONObject();
		 Map paramMap=new HashMap();
		 paramMap.put("progress",progress);
		 String token = req.getHeader("X-Access-Token");
		 if(oConvertUtils.isEmpty(token)) {
			 return Result.error("查看失败，用户不存在！");
		 }
		 String username = JwtUtil.getUsername(token);
		 String id=comInfoService.getByUserName(username);
		 paramMap.put("id",id);
		 paramMap.put("pageNo",pageNo);
		 paramMap.put("pageSize",pageSize);
		 IPage<Map> result=comProgressService.listByKey(paramMap);
		 return Result.ok(result);
	 }
	/**
	 *   添加
	 *
	 * @param comProgress
	 * @return
	 */
	@AutoLog(value = "办理进度-添加")
	@ApiOperation(value="办理进度-添加", notes="办理进度-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ComProgress comProgress) {
		comProgressService.save(comProgress);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param comProgress
	 * @return
	 */
	@AutoLog(value = "办理进度-编辑")
	@ApiOperation(value="办理进度-编辑", notes="办理进度-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ComProgress comProgress) {
		comProgressService.updateById(comProgress);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "办理进度-通过id删除")
	@ApiOperation(value="办理进度-通过id删除", notes="办理进度-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		comProgressService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "办理进度-批量删除")
	@ApiOperation(value="办理进度-批量删除", notes="办理进度-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.comProgressService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "办理进度-通过id查询")
	@ApiOperation(value="办理进度-通过id查询", notes="办理进度-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ComProgress comProgress = comProgressService.getById(id);
		if(comProgress==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(comProgress);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param comProgress
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ComProgress comProgress) {
        return super.exportXls(request, comProgress, ComProgress.class, "办理进度");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ComProgress.class);
    }

}
