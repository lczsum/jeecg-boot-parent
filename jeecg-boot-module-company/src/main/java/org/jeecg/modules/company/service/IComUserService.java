package org.jeecg.modules.company.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.jeecg.modules.company.entity.ComUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Description: 企业人员
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
public interface IComUserService extends IService<ComUser> {
    /**
     * 检索企业员工
     * @param pageNo
     * @param pageSize
     * @param comUser
     * @return
     */
    IPage<ComUser> listByKey(Integer pageNo,Integer pageSize,ComUser comUser);

    /**
     * 查询省
     * @return
     */
    List getProvince();

    /**
     * 根据省id查询城市
     * @param paramMap
     * @return
     */
    List getCityByPid(Map paramMap);

    /**
     * 校验企业法人
     * @param paramMap
     * @return
     */
    boolean checkUnique(Map paramMap);

}
