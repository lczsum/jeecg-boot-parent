package org.jeecg.modules.company.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecg.modules.company.entity.ComQual;
import org.jeecgframework.poi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 企业基本信息表
 * @Author: jeecg-boot
 * @Date:   2020-03-15
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ComInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;

	/**企业id编号*/
    @ApiModelProperty(value = "企业id编号")
    private String id;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@Excel(name = "创建日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
	@Excel(name = "更新人", width = 15)
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@Excel(name = "更新日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**企业名称*/
	@Excel(name = "企业名称", width = 15)
    @ApiModelProperty(value = "企业名称")
    private String companyName;
	/**所属行业*/
	@Excel(name = "所属行业", width = 15)
    @ApiModelProperty(value = "所属行业")
    private Integer industry;
	/**厂房*/
	@Excel(name = "厂房", width = 15)
    @ApiModelProperty(value = "厂房")
    private Integer workshop;
	/**工会*/
	@Excel(name = "工会", width = 15)
    @ApiModelProperty(value = "工会")
    private String labourunion;
	/**党支部*/
	@Excel(name = "党支部", width = 15)
    @ApiModelProperty(value = "党支部")
    private String partybranch;
	/**入驻时间*/
	@Excel(name = "入驻时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "入驻时间")
    private Date intime;
	/**占地面积*/
	@Excel(name = "占地面积", width = 15)
    @ApiModelProperty(value = "占地面积")
    private String arearange;
	/**建筑面积*/
	@Excel(name = "建筑面积", width = 15)
    @ApiModelProperty(value = "建筑面积")
    private String buildrange;
	/**营业执照*/
	@Excel(name = "营业执照", width = 15)
    @ApiModelProperty(value = "营业执照")
    private String businesslicense;
	/**产品大类*/
	@Excel(name = "产品大类", width = 15)
    @ApiModelProperty(value = "产品大类")
    private String productcategory;
	/**产品描述*/
	@Excel(name = "产品描述", width = 15)
    @ApiModelProperty(value = "产品描述")
    private String productdescription;
	/**产品图片*/
	@Excel(name = "产品图片", width = 15)
    @ApiModelProperty(value = "产品图片")
    private String productimg;
	/**编辑时间*/
	@Excel(name = "编辑时间", width = 15)
    @ApiModelProperty(value = "编辑时间")
    private String edittime;
	/**是否删除*/
	@Excel(name = "是否删除", width = 15)
    @ApiModelProperty(value = "是否删除")
    private Integer isdelete;
	/**企业资质*/
	 @ApiModelProperty(value = "企业资质")
	private List<ComQual> comQuals;

	/**资质类型*/
	@Excel(name = "资质类型", width = 15)
    @ApiModelProperty(value = "资质类型")
    private Integer qualtype;
	/**资质名称*/
	@Excel(name = "资质名称", width = 15)
    @ApiModelProperty(value = "资质名称")
    private String qualname;
	/**有效期*/
	@Excel(name = "有效期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "有效期")
    private Date timeliness;
	/**文件地址*/
	@Excel(name = "文件地址", width = 15)
    @ApiModelProperty(value = "文件地址")
    private String filepath;
}
